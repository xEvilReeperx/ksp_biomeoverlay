﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace BiomeOverlay.UI
{
    using BiomeList = List<CBAttributeMap.MapAttribute>;

    public class OptionsWindow : MonoBehaviour
    {
        //---------------------------------------------------------------------
        // Constants
        //---------------------------------------------------------------------
        const int WINDOW_ID = 23666;
        const float WINDOW_DRAG_BORDER = 10f;
        const int WINDOW_MIN_WIDTH = 128;
        const int WINDOW_MIN_HEIGHT = 128;


        //---------------------------------------------------------------------
        // Member values
        //---------------------------------------------------------------------
        Overlay overlay;
        ExperimentSituations selectedSituations = 0;
        OverlayDisplayOptions selectedDisplayOption = OverlayDisplayOptions.All;

        Rect optionsRect = new Rect(Screen.width * 0.05f, Screen.height * 0.5f - 382 / 2, 312, 382);
        
        GUISkin guiSkin;

            // resizing
            Rect resizeHorizontal = new Rect();
            Rect resizeVertical = new Rect();
            Rect resizeBoth = new Rect();
            Rect dragRect = new Rect(0, 0, 10000, 20);

            Texture2D cursorHorizontal;
            Texture2D cursorVertical;
            Texture2D cursorBoth;
            Texture2D cursorMove;
            // note: these textures seem to get corrupted if unity attempts to use them
            // as hardware cursors, so I force software

            #region resize member variables

            [Flags]
            private enum ResizeStatus
            {
                NotResizing = 0x0,
                Horizontal  = 0x1,
                Vertical    = 0x2,
                Both        = Horizontal | Vertical
            }

            ResizeStatus isResizing = ResizeStatus.NotResizing;
            #endregion

            // contents
            ToggleOption[] experimentToggles;
            ToggleOption[] situationToggles;
            RadioButtonSet displayOptions;

            #region situation indices

            private const int TOGGLE_SrfLanded_Index = 0;
            private const int TOGGLE_SrfSplashed_Index = 1;
            private const int TOGGLE_LowInSpace_Index = 2;
            private const int TOGGLE_HighInSpace_Index = 3;
            private const int TOGGLE_LowInAtmos_Index = 4;
            private const int TOGGLE_HighInAtmos_Index = 5;

            #endregion

            // control-related
            Vector2 optionsScrollPosition = new Vector2();      // vertical scroll position of the optionsWindow


            // Biome display possibilities
            private enum OverlayDisplayOptions
            {
                All = 0,
                GainedScienceAll = 1,
                GainedScienceAny = 2,
                NoScienceAll = 3,
                NoScienceAny = 4
            }


        /// <summary>
        /// Callback for ToggleOption
        /// </summary>
        /// <param name="toggled"></param>
        private delegate void ToggleCallback(ToggleOption toggle);


        /// <summary>
        /// Passed to FilterBiomes.  Return true if the given biome should be
        /// masked on the overlay.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="biome"></param>
        /// <returns></returns>
        private delegate bool MaskLogicFunction(ScienceSubject subject, CBAttributeMap.MapAttribute biome);




        /// <summary>
        /// Wrap the bookkeeping info into a class for Toggle buttons
        /// </summary>
        private class ToggleOption
        {
            public GUIContent content;
            public Color textColor;
            public bool toggled = false;
            private List<ToggleCallback> callbacks = new List<ToggleCallback>();


            public ToggleOption(GUIContent contents = null)
            {
                content = contents;
                textColor = Color.white;
            }

            public void AddCallback(ToggleCallback cb)
            {
                callbacks.Add(cb);
            }

            public void Toggle(bool toggle)
            {
                if (toggle != toggled)
                {
                    toggled = toggle;

                    foreach (var cb in callbacks)
                        cb(this);

                    Audio.Instance.PlayClip((toggle ? 0 : 1));
                }
            }

            public virtual void Render(GUILayoutOption[] options = null)
            {
                var prevColor = GUI.color;
                GUI.color = textColor;
                Toggle(GUILayout.Toggle(toggled, content, options));
                GUI.color = prevColor;
            }
        }



        /// <summary>
        /// Exactly the same as a ToggleOption, except that we can include
        /// a reference to a ScienceExperiment
        /// </summary>
        private class ScienceToggle : ToggleOption
        {
            ScienceExperiment experiment;

            public ScienceToggle(GUIContent content, ScienceExperiment relatedExperiment) : base(content)
            {
                experiment = relatedExperiment;
            }

            public ScienceExperiment Experiment
            {
                get
                {
                    return experiment;
                }
            }
        }

        


        /// <summary>
        /// Essentially a collection of toggle options with the restriction 
        /// that only one toggle option may be toggled on at a time.
        /// </summary>
        private class RadioButtonSet
        {
            class RadioButton : ToggleOption
            {
                RadioButtonSet mySet = null;

                // radio button textures
                private static Texture2D Texture;

                // impl
                public RadioButton(RadioButtonSet owner, GUIContent text)
                    : base(text)
                {
                    if (RadioButton.Texture == null)
                    {
                        RadioButton.Texture = GameDatabase.Instance.GetTextureIn("BiomeOverlay/textures/", "radioButton", false);
                        if (RadioButton.Texture == null)
                            Log.Warning("Failed to load RadioButton texture!");
                    }

                    mySet = owner;
                }

                public override void Render(GUILayoutOption[] options = null /* will be ignored anyway */)
                {
                    Rect drawRect;

                    GUILayout.BeginHorizontal();
                        GUILayout.BeginVertical(GUILayout.MaxWidth(RadioButton.Texture.width / 2 + 2 /* a little larger so the label isn't right next to the button */));  
                            // button will be slightly off center compared to text
                            // unless we pop a vertical spacer in there somewhere
                            GUILayout.Space(2f * RadioButton.Texture.height / 16);

                            bool stateChange = GUILayout.Button("", GUILayout.MinWidth(RadioButton.Texture.width / 2), GUILayout.MaxWidth(RadioButton.Texture.width / 2), GUILayout.MinHeight(RadioButton.Texture.height), GUILayout.MaxHeight(RadioButton.Texture.height));

                            // draw the radio button in the space we just created
                            drawRect = GUILayoutUtility.GetLastRect();
                            GUI.DrawTextureWithTexCoords(drawRect, RadioButton.Texture, (toggled ? new Rect(0.5f, 0f, 0.5f, 1f) : new Rect(0f, 0f, 0.5f, 1f)), true);

                        GUILayout.EndVertical();

                        // finish with the actual label
                        GUILayout.Label(content, GUILayout.ExpandWidth(true));

                    GUILayout.EndHorizontal();

                    // did we get clicked?
                    if (stateChange)
                    {
                        // fire any callbacks being listened for
                        // (assuming, of course, that we weren't
                        // toggled before this click)
                        //Log.Verbose("RadioButton {0} was toggled on", content.text);
                        Toggle(true);
                    }
                }
            }

            HashSet<RadioButton> buttons;

            public RadioButtonSet()
            {
                buttons = new HashSet<RadioButton>();
            }

            public void Add(GUIContent radioContent, ToggleCallback callback)
            {
                RadioButton button = new RadioButton(this, radioContent);

                // to this button, we'll assign the specified delegate...
                if (callback != null)
                {
                    button.AddCallback(callback);
                }
                else Log.Warning("radio button {0} callback is null; it won't do anything!", radioContent.text);

                // we'll also add our own delegate which will let us
                // know when the radio button changes
                button.AddCallback(radio =>
                {
                    if (radio.toggled)
                    {
                        foreach (var b in buttons)
                        {
                            //Log.Debug("RadioSet: {0}", b.content.text);

                            if (!ReferenceEquals(radio, b))
                            {
                                //Log.Debug("Disabling it");
                                b.toggled = false;  // no need to raise any callbacks; the base class will handle that
                                //Log.Debug("radio {0} is now off", b.content.text);
                            }
 
                        }
                    }
                });

                if (buttons.Count == 0)
                    button.toggled = true;

                buttons.Add(button);
            }


            /// <summary>
            /// Render all radio buttons in this list
            /// </summary>
            public void Render()
            {
                foreach (var button in buttons)
                    button.Render();
            }  
        }



    //#####################################################################
    //  -- Begin implementation
    //#####################################################################
        public void Initialize(Overlay overlaySphere)
        {
            overlay = overlaySphere;

            // load textures
            cursorBoth = GameDatabase.Instance.GetTextureIn("BiomeOverlay/textures/", "cursor_resize_b", false);
            cursorHorizontal = GameDatabase.Instance.GetTextureIn("BiomeOverlay/textures/", "cursor_resize_h", false);
            cursorVertical = GameDatabase.Instance.GetTextureIn("BiomeOverlay/textures/", "cursor_resize_v", false);
            cursorMove = GameDatabase.Instance.GetTextureIn("BiomeOverlay/textures/", "cursor_move", false);

            if (!cursorBoth) cursorBoth = new Texture2D(32, 32);
            if (!cursorHorizontal) cursorHorizontal = new Texture2D(32, 32);
            if (!cursorVertical) cursorVertical = new Texture2D(32, 32);
            if (!cursorMove) cursorMove = new Texture2D(32, 32);

            CalculateResizeRects();

            // set up skin (mainly we're compacting things a bit)
            guiSkin = GUISkin.Instantiate(GUI.skin) as GUISkin;
            guiSkin.button.margin = new RectOffset(0, 0, 0, 0);
            guiSkin.toggle.margin = new RectOffset(0, 0, 0, 0);
            guiSkin.label.margin = new RectOffset(0, 0, 0, 0);
            guiSkin.verticalScrollbar.margin = new RectOffset(0, 0, 0, 0);
            guiSkin.horizontalScrollbar.margin = new RectOffset(0, 0, 0, 0);
            guiSkin.scrollView.margin = new RectOffset(0, 0, 0, 0);

          
            
            
            #region situation filters

            //--------------------------------------------------
            // situation options

            //--------------------------------------------------
            situationToggles = new ToggleOption[6];
            situationToggles[TOGGLE_SrfLanded_Index] = new ToggleOption(new GUIContent("Landed on Surface"));
            situationToggles[TOGGLE_SrfLanded_Index].AddCallback(t => OnSituationChange(ExperimentSituations.SrfLanded, t.toggled));

            situationToggles[TOGGLE_SrfSplashed_Index] = new ToggleOption(new GUIContent("Splashed Down"));
            situationToggles[TOGGLE_SrfSplashed_Index].AddCallback(t => OnSituationChange(ExperimentSituations.SrfSplashed, t.toggled));

            situationToggles[TOGGLE_LowInSpace_Index] = new ToggleOption(new GUIContent("Low in Space"));
            situationToggles[TOGGLE_LowInSpace_Index].AddCallback(t => OnSituationChange(ExperimentSituations.InSpaceLow, t.toggled));

            situationToggles[TOGGLE_HighInSpace_Index] = new ToggleOption(new GUIContent("High in Space"));
            situationToggles[TOGGLE_HighInSpace_Index].AddCallback(t => OnSituationChange(ExperimentSituations.InSpaceHigh, t.toggled));

            situationToggles[TOGGLE_LowInAtmos_Index] = new ToggleOption(new GUIContent("Low in Atmosphere"));
            situationToggles[TOGGLE_LowInAtmos_Index].AddCallback(t => OnSituationChange(ExperimentSituations.FlyingLow, t.toggled));

            situationToggles[TOGGLE_HighInAtmos_Index] = new ToggleOption(new GUIContent("High in Atmosphere"));
            situationToggles[TOGGLE_HighInAtmos_Index].AddCallback(t => OnSituationChange(ExperimentSituations.FlyingHigh, t.toggled));

            #endregion

            #region biome filters
            //--------------------------------------------------
            // biome display options
            //--------------------------------------------------
            displayOptions = new RadioButtonSet();
            displayOptions.Add(new GUIContent("All"),                                   t => OnDisplayChange(OverlayDisplayOptions.All));
            displayOptions.Add(new GUIContent("Gained science for every experiment"),   t => OnDisplayChange(OverlayDisplayOptions.GainedScienceAll));
            displayOptions.Add(new GUIContent("Gained science, any experiment"),        t => OnDisplayChange(OverlayDisplayOptions.GainedScienceAny));
            displayOptions.Add(new GUIContent("No science on record for all selected"), t => OnDisplayChange(OverlayDisplayOptions.NoScienceAll));
            displayOptions.Add(new GUIContent("No science on record for any selected"), t => OnDisplayChange(OverlayDisplayOptions.NoScienceAny));
            
            

            #endregion

            #region experiment filters

            //--------------------------------------------------
            // experiment filters
            //--------------------------------------------------

            var experimentIDs = ResearchAndDevelopment.GetExperimentIDs().ToArray();
            experimentToggles = new ToggleOption[experimentIDs.Length];

            for (int i = 0; i < experimentIDs.Length; ++i)
            {
                var experiment = ResearchAndDevelopment.GetExperiment(experimentIDs[i]);

                experimentToggles[i] = new ScienceToggle(new GUIContent(experiment.experimentTitle), experiment);
                experimentToggles[i].AddCallback(t => OnExperimentSelectionChange());
            }

            #endregion

            
            RefreshBiomeDisplay();

        }



        /// <summary>
        /// Called every frame
        /// </summary>
        public void OnGUI()
        {
            if (GUI.Button(new Rect(50, 50, 64, 24), new GUIContent("Dump SS Transforms")))
            {
                Log.Verbose("Dumping ScaledSpace transforms: ");
                Log.Verbose("ScaleFactor: {0}", ScaledSpace.Instance.scaleFactor);

                foreach (var t in ScaledSpace.Instance.scaledSpaceTransforms)
                    Log.Verbose("Transform: {0}", t);
            }


            optionsRect = GUILayout.Window(WINDOW_ID, optionsRect, WindowFunc, "Biome Overlay Options");
        }



        #region GUI


        /// <summary>
        /// Recalculate the size of the "hot" zones of the OptionsWindow based
        /// on its current dimensions.
        /// </summary>
        private void CalculateResizeRects()
        {
            // vertical hotzone is at the bottom of the window
            resizeVertical = new Rect(0, optionsRect.height - WINDOW_DRAG_BORDER, optionsRect.width, WINDOW_DRAG_BORDER);
            
            // horizontal hotzone is on the right side of the window
            resizeHorizontal = new Rect(optionsRect.width - WINDOW_DRAG_BORDER, 0, WINDOW_DRAG_BORDER, optionsRect.height);

            // both is a small square on bottom-right of window
            resizeBoth = new Rect(optionsRect.width - WINDOW_DRAG_BORDER, optionsRect.height - WINDOW_DRAG_BORDER, WINDOW_DRAG_BORDER, WINDOW_DRAG_BORDER);

            dragRect.width = optionsRect.width;
        }



        /// <summary>
        /// Window function for OptionsWindow
        /// </summary>
        /// <param name="windowId"></param>
        public void WindowFunc(int windowId)
        {
            GUI.skin = guiSkin;
            Event e = Event.current;

            #region resize code


            // compare against hotspots before checking event
            // type to simplify the cursor code a little
            if (resizeBoth.Contains(e.mousePosition))
            {
                if (e.type == EventType.MouseDown && e.button == 0 /* left mouse only */)
                {
                    isResizing = ResizeStatus.Both;
                    GUIUtility.hotControl = 9999;
                    Log.Debug("Resize both");
                }

                Cursor.SetCursor(cursorBoth, new Vector2(cursorBoth.width / 2, cursorBoth.height / 2), CursorMode.ForceSoftware);
            } else if (resizeHorizontal.Contains(e.mousePosition)) {
                if (e.type == EventType.MouseDown && e.button == 0)
                {
                    isResizing = ResizeStatus.Horizontal;
                    GUIUtility.hotControl = 9999;
                    Log.Debug("Resize horizontal");
                }

                Cursor.SetCursor(cursorHorizontal, new Vector2(cursorHorizontal.width / 2, cursorHorizontal.height / 2), CursorMode.ForceSoftware);
            } else if (resizeVertical.Contains(e.mousePosition)) {
                if (e.type == EventType.MouseDown && e.button == 0)
                {
                    isResizing = ResizeStatus.Vertical;
                    GUIUtility.hotControl = 9999;
                    Log.Debug("Resize vertical");
                }
                Cursor.SetCursor(cursorVertical, new Vector2(cursorVertical.width / 2, cursorVertical.height / 2), CursorMode.ForceSoftware);

            } else {
                if (e.type == EventType.MouseDown)
                    isResizing = ResizeStatus.NotResizing;

                if (dragRect.Contains(e.mousePosition))
                {
                    Cursor.SetCursor(cursorMove, new Vector2(cursorMove.width / 2, cursorMove.height / 2), CursorMode.ForceSoftware);
                }
                else Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            }
            


            if (isResizing != ResizeStatus.NotResizing)
                if (e.type == EventType.mouseUp || Input.GetMouseButtonUp(0))
                {
                    isResizing = ResizeStatus.NotResizing;
                    if (GUIUtility.hotControl == 9999) GUIUtility.hotControl = 0;
                    CalculateResizeRects();
                    Log.Warning("No longer resizing");
                }


            if (isResizing != ResizeStatus.NotResizing)
            {
                if ((isResizing & ResizeStatus.Horizontal) != 0)
                    optionsRect.width = Math.Max(WINDOW_MIN_WIDTH, Input.mousePosition.x - optionsRect.x);

                if ((isResizing & ResizeStatus.Vertical) != 0)
                    optionsRect.height = Math.Max(WINDOW_MIN_HEIGHT, (Screen.height - Input.mousePosition.y) - optionsRect.y);
            }

            if (isResizing != ResizeStatus.NotResizing)
                Log.Debug("Current optionsRect: {0}", optionsRect);

            #endregion

            // only display Biome Name if user desires it
            if (Settings.DISPLAY_BIOME_NAME && overlay != null)
            {
                GUILayout.BeginVertical();
                GUILayout.Label(new GUIContent(String.Format("Biome: {0}", overlay.HighlightedBiome)));
                GUILayout.EndVertical();
            }
            else Log.Warning("sphere is null; no biome name!");


            // Situation Filter
            optionsScrollPosition = GUILayout.BeginScrollView(optionsScrollPosition, false, false, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            GUILayout.BeginVertical();
            GUILayout.Label(new GUIContent("Situation Options"));
                //situationOptions.Render();
                foreach (var toggle in situationToggles)
                    toggle.Render();

            GUILayout.Space(5);



            // Display options
            GUILayout.Label(new GUIContent("Display Options"));
            displayOptions.Render();

            // Experiment Filter
            GUILayout.BeginHorizontal(); // to place "all" selection button
                GUILayout.Label(new GUIContent("Experiment Filter"));
                GUILayout.FlexibleSpace();

            #region Experiment convenience functions
            if (GUILayout.Button(new GUIContent("avail"), GUILayout.MaxWidth(64)))
            {
                foreach (var toggle in experimentToggles)
                    toggle.toggled = false; // initially turn all off

                // select onboard experiments that can be performed
                foreach (var part in FlightGlobals.ActiveVessel.Parts)
                    foreach (var aModule in part.Modules)
                    {
                        ModuleScienceExperiment mod = aModule as ModuleScienceExperiment;

                        if (mod != null)
                        {
                            //.Warning("Found a ModuleScienceExperiment {0}", mod.experiment.id);
                            if (!mod.Deployed)
                                foreach (var toggle in experimentToggles)
                                    if (((ScienceToggle)toggle).Experiment == mod.experiment)
                                        toggle.toggled = true; // don't invoke callback just yet, just set toggle value directly
                        }
                    }

                // all right, now force a refresh
                OnExperimentSelectionChange();
            }
            if (GUILayout.Button(new GUIContent("onboard"), GUILayout.MaxWidth(64)))
            {
                foreach (var toggle in experimentToggles)
                    toggle.toggled = false; // initially turn all off

                // select onboard experiments, even deployed ones
                foreach (var part in FlightGlobals.ActiveVessel.Parts)
                    foreach (var aModule in part.Modules)
                    {
                        ModuleScienceExperiment mod = aModule as ModuleScienceExperiment;
                            
                        if (mod != null)
                        {
                            //Log.Warning("Found a ModuleScienceExperiment {0}", mod.experiment.id);

                            foreach (var toggle in experimentToggles)
                                if (((ScienceToggle)toggle).Experiment == mod.experiment)
                                    toggle.toggled = true; // don't invoke callback just yet, just set toggle value directly
                        }
                    }

                // all right, now force a refresh
                OnExperimentSelectionChange();
            }

            if (GUILayout.Button(new GUIContent("all"), GUILayout.MaxWidth(32)))
            {
                foreach (var toggle in experimentToggles)
                    toggle.toggled = true;

                OnExperimentSelectionChange();
            }
            #endregion
            GUILayout.EndHorizontal();

                foreach (var toggle in experimentToggles)
                    toggle.Render();



            // Settings (todo)
            GUILayout.EndVertical();
            GUILayout.EndScrollView();


            if (isResizing == ResizeStatus.NotResizing)
                GUI.DragWindow();
        }


        #endregion



        /// <summary>
        /// Make the user interface current.  Mainly this is displaying
        /// the various altitudes at which atmosphere and space are at
        /// </summary>
        private void RefreshUserInterface()
        {
            if (overlay.Body == null) return;


            if (!overlay.Body.atmosphere)
            {
                // turn atmosphere toggles an angry red color to let the user
                // know that choosing them is pointless
                situationToggles[TOGGLE_LowInAtmos_Index].textColor = situationToggles[TOGGLE_HighInAtmos_Index].textColor = Color.red;
            }
            else
            {
                situationToggles[TOGGLE_LowInAtmos_Index].textColor = situationToggles[TOGGLE_HighInAtmos_Index].textColor = Color.white;
            }


            if (Settings.DISPLAY_SPACE_ALTITUDES)
            {
                // update atmosphere labels
                situationToggles[TOGGLE_LowInAtmos_Index].content.text = string.Format("Low in Atmosphere < {0}", (!overlay.Body.atmosphere ? "(n/a)" : FormatAltitude(overlay.Body.scienceValues.flyingAltitudeThreshold)));
                situationToggles[TOGGLE_HighInAtmos_Index].content.text = string.Format("High in Atmosphere {0}", (!overlay.Body.atmosphere ? "(n/a)" : string.Format("{0}-{1}", FormatAltitude(overlay.Body.scienceValues.flyingAltitudeThreshold), FormatAltitude(overlay.Body.maxAtmosphereAltitude))));


                // update low in space label
                situationToggles[TOGGLE_LowInSpace_Index].content.text = string.Format("Low in Space < {0}", FormatAltitude(overlay.Body.scienceValues.spaceAltitudeThreshold));
                situationToggles[TOGGLE_HighInSpace_Index].content.text = string.Format("High in Space > {0}", FormatAltitude(overlay.Body.scienceValues.spaceAltitudeThreshold));

                if (overlay.Body.scienceValues == null)
                    Log.Error("scienceValues == null!");

                Log.Debug("Low in space: < {0} for {1}", overlay.Body.scienceValues.spaceAltitudeThreshold, overlay.Body.name);
            }


            // mark any experiments that don't apply in ANY of the selected
            // situations as red
            if (SelectedSituationList.Count > 0)
                foreach (var toggle in experimentToggles)
                {
                    ScienceToggle sciToggle = toggle as ScienceToggle; // should always work

                    //if (SelectedSituationList.Any(sit => sciToggle.Experiment.IsAvailableWhile(sit, sphere.Body) && sciToggle.Experiment.BiomeIsRelevantWhile(sit)))
                    if (sciToggle.Experiment.IsAvailableWhile(selectedSituations, overlay.Body) && sciToggle.Experiment.BiomeIsRelevantWhile(selectedSituations))
                    {
                        // the selected experiment is used in at least one of the
                        // situations the user selected
                        sciToggle.textColor = Color.white;
                        sciToggle.content.text = sciToggle.Experiment.experimentTitle;
                        sciToggle.content.tooltip = "BANANA!";

                        //Log.Debug("Experiment {0} is valid", sciToggle.Experiment.id);

                    } else
                    {
                        sciToggle.textColor = Color.red;
                        sciToggle.content.text = string.Format("{0} - {1}", sciToggle.Experiment.experimentTitle, (SelectedSituationList.Any(sit => sciToggle.Experiment.BiomeIsRelevantWhile(sit) && sciToggle.Experiment.IsAvailableWhile(sit, overlay.Body)) ? "conditions" : "biome"));

#if DEBUG
                        //Log.Error("Experiment {0} isn't used under any of the given situations", sciToggle.Experiment.id);
#endif
                    }
                }
        }



        /// <summary>
        /// Here, any calls which actually modify the overlay sphere are
        /// made.
        /// </summary>
        private void RefreshBiomeDisplay()
        {
            RefreshUserInterface();

            if (overlay.Body == null || overlay.Body.BiomeMap == null || overlay.Body.BiomeMap.Map == null)
            {
                Log.Verbose("OptionsWindow.OnDisplayChange: Sphere does not have a valid Body");
                return;
            }

            // if no situations are selected, hide nothing
            if (SelectedSituationList.Count == 0 || overlay.Biomes.Count == 0)
            {
                Log.Verbose("No selected situations or body has no biomes");
                overlay.MaskBiomes();
                return;
            }

            Log.Verbose("Display selection changed");

            List<CBAttributeMap.MapAttribute> targetBiomes;

            try
            {
                switch (SelectedDisplayMode)
                {
                    case OverlayDisplayOptions.All:
                        Log.Verbose("Display all biomes");
                        targetBiomes = new List<CBAttributeMap.MapAttribute>();    // don't mask any biomes
                        break;

                    // No science on record for any selected
                    case OverlayDisplayOptions.NoScienceAny:
                        Log.Verbose("Hide biomes for which any science has been done for any selected experiment");
                        targetBiomes = NoScienceAnySelectedExperiment();
                        break;

                    case OverlayDisplayOptions.NoScienceAll:
                        Log.Verbose("Show only biomes in which all selected experiments have zero science for all given situations.");
                        targetBiomes = NoScienceAllSelectedExperiments();
                        break;

                    // gained science in any selected experiment
                    case OverlayDisplayOptions.GainedScienceAny:
                        Log.Verbose("Show biomes in which any selected experiment gained science");
                        targetBiomes = GainedScienceAnySelectedExperiment();
                        break;

                    // show biomes in which every experiment has submitted science data
                    case OverlayDisplayOptions.GainedScienceAll:
                        Log.Verbose("Show biomes in which all selected experiments gained science");
                        targetBiomes = GainedScienceAllSelectedExperiments();
                        break;

                    default:
                        targetBiomes = new List<CBAttributeMap.MapAttribute>(); // mask nothing
                        Log.Error("OptionsWindow.RefreshBiomeDisplay - unhandled display mode selection");
                        break;
                }
            } catch (Exception e)
            {
                Log.Error("An exception occurred in RefreshBiomeDisplay: {0}", e);
                targetBiomes = new List<CBAttributeMap.MapAttribute>();
            }

            if (SelectedSituationList.Count > 0)
                Log.Verbose("Under these situations:");

            foreach (var s in SelectedSituationList)
                Log.Verbose("  - {0}", s);

            if (SelectedExperiments.Count > 0)
                Log.Verbose("with these experiments:");

            foreach (var e in SelectedExperiments)
                Log.Verbose("  - {0}", e.id);

            if (targetBiomes.Count > 0)
                Log.Verbose("These biomes are to be masked:");

            foreach (var b in targetBiomes)
                Log.Verbose("   - {0} ({1})", b.name, b.mapColor);

            // let sphere know what it should be hiding from view
            overlay.MaskBiomes(targetBiomes);



        }


        // Hide biomes which which any science has been done for any selected experiment
        // we want only "fresh, unresearched" biomes to be visible
        private BiomeList NoScienceAnySelectedExperiment()
        {
            var targets = new BiomeList();

            foreach (var experiment in SelectedExperiments)
                foreach (var situation in SelectedSituationList)
                {
                    if (experiment.BiomeIsRelevantWhile(situation))
                        if (experiment.IsAvailableWhile(situation, overlay.Body))
                            foreach (var biome in overlay.Biomes)
                            {
                            // any science on this biome?
                                var subject = ResearchAndDevelopment.GetExperimentSubject(experiment, situation, overlay.Body, biome.name);
                                if (subject.science > 0f)
                                {
                                    targets.Add(biome);
                                    break;
                                }
                            }

                }

            return targets;
        }


        /// <summary>
        /// Show only biomes in which all selected experiments have zero science 
        /// for all given situations
        /// </summary>
        /// <returns></returns>
        private BiomeList NoScienceAllSelectedExperiments()
        {
            var targets = overlay.Biomes;

            foreach (var biome in overlay.Biomes)
            {
                if (SelectedExperiments.All(experiment =>
                    SelectedSituationList.All(situation =>
                        {
                            if (experiment.BiomeIsRelevantWhile(situation) && experiment.IsAvailableWhile(situation, overlay.Body))
                                return ResearchAndDevelopment.GetExperimentSubject(experiment, situation, overlay.Body, biome.name).science < 0.0005f;

                            return false;
                        })))
                {
                    targets.Remove(biome);
                }
            }

            return targets;
        }



        private BiomeList GainedScienceAnySelectedExperiment()
        {
            var targets = overlay.Biomes;

            // if any experiment + situation + biome combo has submitted science,
            // remove its biome from the target list
            foreach (var experiment in SelectedExperiments)
                foreach (var situation in SelectedSituationList)
                {
                    if (experiment.BiomeIsRelevantWhile(situation))
                        if (experiment.IsAvailableWhile(situation, overlay.Body))
                            foreach (var biome in targets)
                            {
                                var subject = ResearchAndDevelopment.GetExperimentSubject(experiment, situation, overlay.Body, biome.name);

                                if (subject.science > 0f)
                                {
                                    targets.Remove(biome);
                                    break;
                                }
                            }
                }

            return targets;
        }



        // show biomes in which every experiment has submitted science data in every selected situation
        private BiomeList GainedScienceAllSelectedExperiments()
        {
            var targets = overlay.Biomes;

            foreach (var biome in overlay.Biomes)
            {
                if (SelectedSituationList.All(situation =>
                        SelectedExperiments.All(experiment =>
                        {
                            if (experiment.BiomeIsRelevantWhile(situation))
                                if (experiment.IsAvailableWhile(situation, overlay.Body))
                                    return ResearchAndDevelopment.GetExperimentSubject(experiment, situation, overlay.Body, biome.name).science > 0;

                            return false;
                        })))
                {
                    targets.Remove(biome);
                }
            }

            return targets;
        }



        /// <summary>
        /// User has selected a different situation to filter overlay
        /// for.  If add is true, the supplied situation has been added
        /// to the filter.  Otherwise it should be excluded instead
        /// </summary>
        /// <param name="situation"></param>
        private void OnSituationChange(ExperimentSituations situation, bool add = true)
        {
            if (add)
            {
                selectedSituations |= situation;
            }
            else
            {
                selectedSituations = selectedSituations & ~situation;
            }

            Log.Verbose("Situation selection changed ({1}): {0}", Enum.GetName(typeof(ExperimentSituations), situation), (add ? "added" : "removed"));

            var values = Enum.GetValues(typeof(ExperimentSituations));

            foreach (var sit in SelectedSituationList)
                Log.Verbose("  situation '{0}' is selected", sit);

            RefreshBiomeDisplay();
        }



        /// <summary>
        /// User wants to change the display mode of the overlay
        /// </summary>
        private void OnDisplayChange(OverlayDisplayOptions newOption)
        {
            selectedDisplayOption = newOption;
            RefreshBiomeDisplay();
        }



        /// <summary>
        /// Hide biomes based on the provided function
        /// </summary>
        /// <returns></returns>
        private List<CBAttributeMap.MapAttribute> FilterBiomes(MaskLogicFunction fn)
        {
            var maskBiomes = new List<CBAttributeMap.MapAttribute>();

            foreach (var situation in SelectedSituationList)
                foreach (var biome in overlay.Biomes)
                    foreach (var experiment in SelectedExperiments)
                    {
                        if (!experiment.BiomeIsRelevantWhile(situation) || !experiment.IsAvailableWhile(situation, overlay.Body) || maskBiomes.Contains(biome))
                            continue; // experiment either isn't relevant or isn't available at this situation

                        ScienceSubject science = ResearchAndDevelopment.GetExperimentSubject(experiment, situation, overlay.Body, biome.name);

                        if (fn(science, biome))
                            maskBiomes.Add(biome);
                    }

            return maskBiomes;
        }


        #region delegates


        /// <summary>
        /// Mask any biome which has science accumulated for this subject
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="biome"></param>
        /// <returns></returns>
        private bool ExcludeResearched(ScienceSubject subject, CBAttributeMap.MapAttribute biome)
        {
            if (subject.science > 0f)
                Log.Warning("Masking subject {0} because science > 0", subject.id);

            return subject.science > 0f; // will be excluded if has science
        }



        /// <summary>
        /// Mask any biome in which no science has been done in the given
        /// subject
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="biome"></param>
        /// <returns></returns>
        private bool IncludeOnlyResearched(ScienceSubject subject, CBAttributeMap.MapAttribute biome)
        {
            return subject.science < 0.0005f; // will be included if zero science
        }



        /// <summary>
        /// Our owner (BiomeOverlay) will call us when the situation 
        /// changes.  Appropriate response goes here
        /// </summary>
        /// <param name="mapVisible"></param>
        /// <param name="guiVisible"></param>
        public void OnMapViewChange(bool mapVisible, bool guiVisible)
        {
            Log.Verbose("OptionsWindow OnMapViewChange: map {0}, gui {1}", mapVisible, guiVisible);
            RefreshBiomeDisplay();
            //Visible = mapVisible && guiVisible;
        }

        #endregion

        #region Properties
        /// <summary>
        /// User has changed an experiment selection
        /// </summary>
        private void OnExperimentSelectionChange()
        {
            Log.Verbose("Experiment selection changed");
            RefreshBiomeDisplay();
        }
    


        /// <summary>
        /// Property which returns a list of all selected experiments, as you
        /// probably guessed.  
        /// 
        /// However, experiments that require an atmosphere when the sphere's
        /// body has none will NOT be returned unless a situation in which
        /// they're valid is included
        /// </summary>
        private List<ScienceExperiment> SelectedExperiments
        {
            get
            {
                var list = new List<ScienceExperiment>();

                foreach(var toggle in experimentToggles)
                {
                    ScienceToggle st = toggle as ScienceToggle;

                    if (st.toggled)
                        if (SelectedSituationList.Any(sit => st.Experiment.IsAvailableWhile(sit, overlay.Body)))
                            list.Add(st.Experiment);

                }

                return list;
            }
        }



        /// <summary>
        /// Returns a list containing every selected situation.  A list
        /// containing one entry per situation is simpler to work with
        /// in the display logic.
        /// </summary>
        private List<ExperimentSituations> SelectedSituationList
        {
            get
            {
                var possibilities = Enum.GetValues(typeof(ExperimentSituations));
                var selected = new List<ExperimentSituations>();

                foreach (var situationValue in possibilities)
                {
                    ExperimentSituations thisSituation = (ExperimentSituations)Enum.ToObject(typeof(ExperimentSituations), situationValue);

                    if ((selectedSituations & thisSituation) == thisSituation)
                        selected.Add(thisSituation);
                }

                return selected;
            }
        }


        private OverlayDisplayOptions SelectedDisplayMode
        {
            get
            {
                return selectedDisplayOption;
            }
        }
        #endregion



        /// <summary>
        /// Turn a distance in meters into a more readable form.  Because
        /// "Low Orbit < 1e9" is a sucky description.
        /// </summary>
        /// <param name="altitude"></param>
        /// <returns></returns>
        private string FormatAltitude(float altitude)
        {
            string[] units = {  "m",        // <10^3
                                "km",       // 10^3
                                "Mm",       // 10^6
                                "Gm",       // 10^9
                                "Tm",       // 10^12
                                "Pm"        // 10^15, which really should be more than we can possibly use
                             };

            int degree = (int)Math.Floor(Mathf.Log10(Mathf.Abs(altitude)) / 3);
            altitude *= Mathf.Pow(1000, -degree);

            return string.Format("{0} {1}", altitude, degree >= units.Length ? units[units.Length - 1] : units[degree]);
        }



        /// <summary>
        /// Mainly this is used to red flag experiments that would otherwise be
        /// valid but are invalid with the given situation filter.  For example,
        /// "Low in atmosphere" + crewReport is a valid combination, but not
        /// on the Mun because the Mun has no atmosphere.  etc
        /// </summary>
        /// <param name="oneSituation"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        //private bool AllSituationsAvailable(ExperimentSituations oneSituation, CelestialBody body)
        //{
        //    if (body.BiomeMap == null)
        //        return false;

        //    if (!body.atmosphere)
        //        return ((oneSituation & (ExperimentSituations.FlyingHigh | ExperimentSituations.FlyingHigh)) == 0);

        //    return true;
        //}
    }
}
