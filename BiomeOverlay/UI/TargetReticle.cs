﻿#define DISABLE_WIREFRAME
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BiomeOverlay.UI
{
    class TargetReticle 
    {
        private CelestialBody referenceBody = null;
        private GameObject markerReticle; // (parent of object)
        private GameObject markerObject;            // For some reason, Unity will place
                                                    // an animated object at (0,0,0) relative
                                                    // to its parent.  That's not good when
                                                    // we're trying to place it at a point on
                                                    // the planet's surface.  Instead, it'll
                                                    // be a child and we'll manipulate the parent's
                                                    // transform instead

        public TargetReticle()
        {
            Log.Verbose("TargetReticle: Creating reticle");

            // for some reason, markerPin seems to ignore any rotation (or scale)
            // I give it.  Had to make a parent object to control it with.
            markerReticle = new GameObject("BiomeOverlay.Reticle.Parent");
            markerObject = GameDatabase.Instance.GetModel("BiomeOverlay/models/reticle");

            if (markerObject == null)
            {
                Log.Error("Marker object not found");
            }
            else
            {
                Log.Verbose("Marker pin model located; applying properties");

                if (markerObject.renderer == null)
                {
                    Log.Error("markerPin has no renderer!");
                    return;
                }

                var pinScale = new Vector3(markerObject.transform.localScale.x, markerObject.transform.localScale.y, markerObject.transform.localScale.z);
                Log.Warning("Stored pin scale: {0}", pinScale);

                Material material = Settings.LocateMaterial("BiomeOverlay.Resources.MinimumAmbient.txt", "KSP/Unlit", "Diffuse");
                material.SetFloat("_MinLuminance", 0.60f);

                markerObject.renderer.material = material;
                markerObject.renderer.castShadows = true;
                markerObject.renderer.enabled = true;
                markerObject.layer = Overlay.MAP_PLANET_LAYER;
                markerObject.transform.parent = markerReticle.transform;

                markerReticle.transform.localScale = Vector3.one;
                markerReticle.layer = Overlay.MAP_PLANET_LAYER;

#if DEBUG
#if !DISABLE_WIREFRAME
                markerObject.AddComponent<WireFrameLineRenderer>().Visible = true;
#endif
#endif
                markerObject.SetActive(true);
                markerReticle.SetActive(false);



                if (markerObject.animation)
                {
                    if (markerObject.animation["Spin"])
                    {
                        markerObject.animation["Spin"].speed = Settings.RETICLE_ANIMATION_SPEED;
                    }
                    else Log.Warning("No animation named \"Spin\" found!");
                }
                else Log.Warning("Targeting reticle: no animation found");

                Log.Warning("animation speed is currently {0}, settings has it at {1}", markerObject.animation["Spin"].speed, Settings.RETICLE_ANIMATION_SPEED);

            }

            GameEvents.onGameSceneLoadRequested.Add(OnGameSceneChange);
        }



        ~TargetReticle()
        {
            GameEvents.onGameSceneLoadRequested.Remove(OnGameSceneChange);
            if (markerReticle) GameObject.Destroy(markerReticle); // should take care off child markerObject as well
        }


        public void OnGameSceneChange(GameScenes newScene)
        {
            try
            {
                ScaledSpace.Instance.scaledSpaceTransforms.RemoveAll(t => t == markerReticle.transform);
            } catch {}

        }

        /// <summary>
        /// The reticle's position is updated every frame the user is
        /// in the MapView.
        /// </summary>
        /// <param name="coords"></param>
        public void UpdateReticle(Overlay.Coordinates coords = null)
        {
            if (coords != null && referenceBody)
            {
                Vector3d up = referenceBody.GetSurfaceNVector(coords.latitude, coords.longitude);
                Vector3d center = referenceBody.position + 1.025 * up * referenceBody.Radius;

                markerReticle.transform.rotation = Quaternion.LookRotation(up) * Quaternion.AngleAxis(90, Vector3.right);

                if (referenceBody.pqsController != null)
                    center = referenceBody.position + 1.025 * referenceBody.pqsController.GetSurfaceHeight(QuaternionD.AngleAxis(coords.longitude, Vector3d.down) * QuaternionD.AngleAxis(coords.latitude, Vector3d.forward) * Vector3d.right) * up;

                markerReticle.transform.position = ScaledSpace.LocalToScaledSpace(center);
                markerReticle.SetActive(true);

                //Log.Verbose("Reticle position: {0}", center);
                //Log.Verbose("Body position: {0}", referenceBody.transform.position);
            }
            else
            {
                markerReticle.SetActive(false);
            }
        }




        public void Assign(CelestialBody body)
        {
            referenceBody = body;

            if (referenceBody == null)
            {
                //markerReticle.transform.parent = null;
                markerReticle.transform.localScale = Vector3d.one;
            }
            else
            {
                markerReticle.transform.localScale = Vector3d.one * Settings.RETICLE_SCALE * (referenceBody.Radius / 160000d);

                // if the game scene is changed (from flight->flight) where the reticle
                // isn't destroyed, its scaled space transform might've been removed as a precaution.
                // we'll have to put it back
                if (!ScaledSpace.Instance.scaledSpaceTransforms.Contains(markerReticle.transform))
                    ScaledSpace.AddScaledSpaceTransform(markerReticle.transform);

                //Log.Verbose("markerReticle lossyScale = {0}, localScale = {1},{2},{3}, parentScale = {4},{5},{6}", markerReticle.transform.lossyScale, markerReticle.transform.localScale.x, markerReticle.transform.localScale.y, markerReticle.transform.localScale.z, referenceBody.transform.localScale.x, referenceBody.transform.localScale.y, referenceBody.transform.localScale.z);
                Log.Verbose("TargetReticle assigned to {0}", referenceBody.name);
            }
        }


        #region properties
        public bool Visible
        {
            get
            {
                return markerReticle.activeInHierarchy;
            }

            set
            {
                markerReticle.SetActive(value);
                Log.Debug("Reticle is now {0}", (value ? "visible" : "invisible"));
            }
        }

#if DEBUG
        public bool Wireframe
        {
            get
            {
#if !DISABLE_WIREFRAME
                return markerReticle.GetComponent<WireFrameLineRenderer>().Visible;
#else
                return false;
#endif
            }

            set
            {
#if !DISABLE_WIREFRAME
                markerReticle.GetComponent<WireFrameLineRenderer>().Visible = value;        
#endif
            }
        }
#endif
        #endregion
    }
}
