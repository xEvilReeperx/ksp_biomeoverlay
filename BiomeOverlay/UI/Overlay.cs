﻿#define GENERATE_SPHERE // disabling this is deprecated
#define DISABLE_WIREFRAME

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using KSP.IO;
using System.IO;


namespace BiomeOverlay
{
    
    /// <summary>
    /// Overlay consists of a sphere that encompasses a celestial body plus
    /// a targeting reticle to more clearly indicate to the player which
    /// biome lies on an exact location.
    /// </summary>
    public class Overlay : MonoBehaviour {
        // Sphere itself is the GO we're attached to

        // Const values
        public const int MAP_PLANET_LAYER = 10;
        private const double ROTATION_CORRECTION = 90d * (Math.PI / 180d);
        private const float FLOAT_EPSILON = 0.0005f;
        private const float DEFAULT_COLOR_DIFFERENCE_THRESHOLD = 0.05f;

        
        // flags
        private enum OverlayBackgroundState
        {
            Idle = 0,
            GeneratingBiomeMap,
            DeformingSphere
        }

        private enum OverlayMode
        {
            Normal = 0,                     // no mouse down events
            Inspecting                      // mouse down event on the overlay sphere
        }

        private OverlayBackgroundState state = OverlayBackgroundState.Idle;


        // Targeting reticle for planet
        UI.TargetReticle reticle;                   // not a child GO because we don't want any scale applied to our sphere
                                                    // to factor into reticle's scale

        private string currentBiome = "";           // Highlighted biome name (if one is highlighted)
        private Material biomeMaskMaterial;         // used to hide biome colors in a texture
                                                    // in the render target method of masking

        private Material materialOverlay;           // used in normal mode overlay
        private Material materialInspection;        // when on mousedown and user is inspecting a particular biome


        /// <summary>
        /// Rather than re-deform the default sphere very time the view
        /// changes, cache ones we've completed already
        /// </summary>
        private class CBCache
        {
            public Texture2D map;            // Instead of copying the original biome map which has
                                             // some filtering done to it which can produce artifacts
                                             // in the overlay shader, generate a nice fresh biome map
                                             // by reversing uv coordinates into lat and lon and querying
                                             // the original biome map
       
            public Mesh      mesh;

            public void Release()
            {
                if (map) UnityEngine.Resources.UnloadAsset(map);
                if (mesh) GameObject.Destroy(mesh);
            }
        }

        Dictionary<CelestialBody, CBCache> cache = new Dictionary<CelestialBody, CBCache>();



        CelestialBody   referenceBody = null;
        Transform       referenceScaledTransform = null; // scaledSpace transform of body
        Mesh            sphereMesh = null;          // keeping a separate copy of the sphere mesh available
                                                    // will help us avoid some costly operations later when
                                                    // switching between bodies


        delegate bool IsInListFunc(Color32 pixel);

        //---------------------------------------------------------------------
        // Supporting objects
        //---------------------------------------------------------------------
        public class Coordinates
        {
            public double latitude;
            public double longitude;

            public Coordinates(double latitude, double longitude)
            {
                this.latitude = latitude;
                this.longitude = longitude;
            }
        }



    //#####################################################################
    //  -- Begin implementation
    //#####################################################################



        /// <summary>
        /// Constructor
        /// </summary>
        public void Start() { //Overlay() {
            Log.Verbose("Creating sphere mesh");

#if GENERATE_SPHERE
            sphereMesh = GenerateSphere(1f, Settings.SPHERE_LONG_SLICES, Settings.SPHERE_LAT_SLICES);

            gameObject.AddComponent<MeshFilter>();
            gameObject.AddComponent<MeshRenderer>();
#else
            sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            var renderer = sphere.GetComponent<MeshRenderer>();

            sphereMesh = GameObject.Instantiate(sphere.GetComponent<MeshFilter>().mesh) as Mesh;
#endif
            renderer.enabled = true;
            renderer.castShadows = false;
            renderer.receiveShadows = false;
            gameObject.layer = MAP_PLANET_LAYER;

            Log.Verbose("Adjusting sphere vertex locations");
            Vector3[] vertices = sphereMesh.vertices;

            // the UVs of the sphere we just generated and
            // the planets don't quite align.  Moving the vertices
            // now will make deforming the sphere later much simpler
            // when we're adjusting its contours to better match a body
            for (int i = 0; i < sphereMesh.vertexCount; ++i)
            {
                Vector3 old = vertices[i];

                vertices[i].x = (float)(old.x * Math.Cos(ROTATION_CORRECTION) - old.z * Math.Sin(ROTATION_CORRECTION));
                vertices[i].z = (float)(old.x * Math.Sin(ROTATION_CORRECTION) + old.z * Math.Cos(ROTATION_CORRECTION));

#if !GENERATE_SPHERE
                vertices[i].Normalize();
#endif
            }

            sphereMesh.vertices = vertices;
            sphereMesh.RecalculateBounds();

#if !GENERATE_SPHERE
    sphere.GetComponent<MeshFilter>().sharedMesh = GameObject.Instantiate(sphereMesh) as Mesh;
#endif

            #region Overlay shader
            Log.Verbose("Locating overlay shader");

            materialOverlay = Settings.LocateMaterial("BiomeOverlay.Resources.HighlightShader.txt", "Unlit/Transparent", "Diffuse");

            if (materialOverlay == null)
            {
                Log.Error("No suitable material found for the overlay sphere.  Check installation");
            }
            else
            {
                materialOverlay.SetColor("_HighlightColor", Settings.OVERLAY_HIGHLIGHT_COLOR);
                materialOverlay.SetColor("_AlternateHighlightColor", Settings.OVERLAY_ALTERNATE_HIGHLIGHT_COLOR);
                materialOverlay.SetFloat("_Alpha", Settings.OVERLAY_HIGHLIGHT_ALPHA);
                materialOverlay.SetColor("_TargetColor", Color.clear); // disable highlighting atm
                materialOverlay.SetColor("_Tolerance", new Color(0.02f, 0.02f, 0.02f, 0.02f));
                materialOverlay.SetFloat("_Frequency", Settings.OVERLAY_FREQUENCY);

                renderer.sharedMaterial = materialOverlay;
            }
            #endregion

            #region Overlay click shader
            Log.Verbose("Locating BiomeEdgeDetect shader");

            materialInspection = Settings.LocateMaterial("BiomeOverlay.Resources.BiomeEdgeDetect.txt", "Unlit/Transparent", "Diffuse");

            if (materialInspection == null)
            {
                Log.Error("Failed to locate material for overlay inspection mode.");
            }
            else
            {

                materialInspection.SetColor("_HighlightColor", Settings.OVERLAY_INSP_HIGHLIGHT_COLOR);
                materialInspection.SetColor("_AlternateHighlightColor", Settings.OVERLAY_INSP_ALT_HIGHLIGHT_COLOR);
                materialInspection.SetColor("_TargetColor", Color.clear); // disable highlighting atm
                materialInspection.SetColor("_Tolerance", new Color(0.02f, 0.02f, 0.02f, 0.02f));
                materialInspection.SetFloat("_Frequency", Settings.OVERLAY_INSP_FREQUENCY);

                var bm = FlightGlobals.Bodies.Find(body => body.BiomeMap.Map != null && body.pqsController != null).BiomeMap.Map;

                materialInspection.SetFloat("_Width", (float)bm.width);
                materialInspection.SetFloat("_Height", (float)bm.height);

                Log.Debug("materialInspection: {0}x{1}", bm.width, bm.height);
            }
            #endregion

            #region Target color to transparent shader
            Log.Verbose("Locating TargetColorToTransparent shader");

            biomeMaskMaterial = Settings.LocateMaterial("BiomeOverlay.Resources.TargetColorToTransparent.txt");

            if (biomeMaskMaterial == null && Settings.USE_RENDERTARGET_METHOD)
            {
                Log.Error("Failed to find biome masking shader!  Forcing software pixel masking");
                Settings.USE_RENDERTARGET_METHOD = false;
            }

            #endregion

#if DEBUG
#if !DISABLE_WIREFRAM
            Log.Verbose("Attaching wireframe component to sphere.");
            //material.color = new Color(1.0f, 0.0f, 1.0f, 0.33f); // broken currently
            gameObject.AddComponent<WireFrameLineRenderer>().Visible = false;
#endif
#endif
            Log.Verbose("Created sphere mesh.");

            // create initial entries for Biome Dict
            var list = FlightGlobals.Bodies.Where(body => body.BiomeMap != null).ToList();
            Log.Verbose("Found {0} bodies to cache.", list.Count);

            foreach (var body in list)
                cache.Add(body, new CBCache());


            // create target reticle
            reticle = new UI.TargetReticle();

            // start with sphere and reticle off
            Visible = false;

            // register for events
            GameEvents.onPlanetariumTargetChanged.Add(OnPlanetariumTargetChange);
            GameEvents.onGameSceneLoadRequested.Add(OnGameSceneChangeRequested);

            Log.Verbose("Assigning sphere {0} initially.", FlightGlobals.ActiveVessel.mainBody.name);
            Assign(FlightGlobals.ActiveVessel.mainBody);
        }



        public void OnGameSceneChangeRequested(GameScenes newScene)
        {
            if (newScene == GameScenes.FLIGHT)
                gameObject.transform.parent = null; // otherwise we'll be destroyed alongside the object we're attached to
        }



        /// <summary>
        /// Destructor
        /// </summary>
        public void OnDestroy()
        {
            Log.Verbose("Overlay sphere being destroyed");

            GameEvents.onPlanetariumTargetChanged.Remove(OnPlanetariumTargetChange);
            GameEvents.onGameSceneLoadRequested.Remove(OnGameSceneChangeRequested);

            if (sphereMesh) GameObject.Destroy(sphereMesh);
            foreach (var items in cache)
                items.Value.Release();

            if (reticle != null) reticle = null;
        }


        public void Assign(CelestialBody body)
        {
            if (referenceBody == body)
                return;

            if (IsBusy)
            {
                StopCoroutine("ApplyAssignment");
                state = OverlayBackgroundState.Idle;
            }

            StartCoroutine("ApplyAssignment", body);
        }


        /// <summary>
        /// Assign sphere to the selected body
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        private System.Collections.IEnumerator ApplyAssignment(CelestialBody body) {
            if (referenceBody == body)
            {
                Log.Debug("Overlay already assigned to {0}", body.theName);
                yield break;
            }

            Log.Verbose("Assigning overlay to {0}", body.name);
            referenceBody = body;
            referenceScaledTransform = ScaledSpace.Instance.scaledSpaceTransforms.Find(t => t.name == body.name);

            Log.Verbose("Assigning reticle");
            reticle.Assign(body);

            Log.Debug("Overlay: has been assigned to {0}", body.name);

            if (body.BiomeMap.Map == null)
            {
                Log.Error("Target body does not appear to have a biome map!");
                Visible = false;
                yield break;
            }


            // do we have a cached version of this biome map?
            Texture2D map = cache[body].map;

            if (!map)
            {
                Log.Verbose("No cached textures found for {0}; creating...", body.theName);
                state = OverlayBackgroundState.GeneratingBiomeMap;

                // generate a copy of the biome map of body we're near
                var bmg = new Support.BiomeMapGenerator();

                System.Collections.IEnumerator process = bmg.Create(body);

                while (process.MoveNext())
                {
                    Log.Verbose("Still working on BiomeMap generation...");
                    yield return 0;
                }

                map = cache[body].map = bmg.Result;

                if (Settings.OUTPUT_DEBUG_IMAGES)
                {
                    Log.Warning("Writing debug images to disk");
                    bmg.Result.SaveToDisk(string.Format("{0}.png", body.name));
                }

                Log.Verbose("Cleaned and unfiltered textures created.");
            }
            else
            {
                // have cached version already
                Log.Verbose("Found cached textures for {0}", body.theName);
            }
            
            
            // assign material to sphere
            //if (gameObject.renderer.material.mainTexture != null)
            //    UnityEngine.Texture.Destroy(gameObject.renderer.material.mainTexture);

            //gameObject.renderer.material.mainTexture = GameObject.Instantiate(map) as Texture;


            materialInspection.mainTexture = materialOverlay.mainTexture = gameObject.renderer.material.mainTexture = map;


            // adjust transform and deformation of sphere
            System.Collections.IEnumerator meshUpdateProgress = UpdateMesh(body);

            while (meshUpdateProgress.MoveNext())
            {
                Log.Verbose("Still working on mesh ...");
                yield return 0;
            }

            state = OverlayBackgroundState.Idle;
        }




        /// <summary>
        /// Copy pixels from src into an ARGBA32 format texture
        /// of the same dimensions
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        private Texture2D GenerateARGB32Copy(Texture src) {
            if (src == null)
            {
                Log.Error("Cannot copy source texture; it does not exist!");
                return null;
            }
            Log.Verbose("Creating copy of an {0}x{1} texture...", src.width, src.height);

            // Biome maps are in DXT1 format -- no support for alpha channel
            Texture2D src2d = src as Texture2D;
            Texture2D mainTex = new Texture2D(src.width, src.height, TextureFormat.ARGB32, false);
            mainTex.filterMode = FilterMode.Point;

            Log.Debug("Copy contents of source texture into new ARGB32 texture.");
            Color32[] srcPix = src2d.GetPixels32(0);
            Color32[] destPix = mainTex.GetPixels32();

            if (srcPix == null || destPix == null)
            {
                Log.Error("source or dest pixel buffer null!");
                return null;
            }

            for (int indx = 0; indx < src.width * src.height; ++indx)
                destPix[indx] = srcPix[indx];

            Log.Debug("Upload changes to GPU");
            mainTex.SetPixels32(destPix);
            mainTex.Apply();
            Log.Debug("Done");

            return mainTex;
        }


        /// <summary>
        /// Deform the current sphere mesh so that it more closely fits
        /// the contours of the target body.  Stores past deformations
        /// in a cache, which it will use instead if available
        /// </summary>
        /// <param name="body"></param>
        private System.Collections.IEnumerator UpdateMesh(CelestialBody body)
        {
            Log.Verbose("Updating sphere mesh to match {0}", body.theName);
            var _profile = new ProfileTimer("UpdateMesh vertices");

            // tweak vertex positions
            MeshFilter mf = GetComponent<MeshFilter>();

            if (!mf)
            {
                Log.Error("No MeshFilter attached to overlay sphere");
                yield break;
            }


            // deform sphere vertices
            Mesh mesh = cache[body].mesh;

            if (mesh != null)
            {
                Log.Verbose("Using cached mesh for {0}", body.theName);

                // we can use cached deformed mesh
                mf.sharedMesh = mesh;

            }
            else
            {
                // we'll have to create a copy of the
                // template sphere and deform it since
                // it hasn't been cached
                Log.Verbose("No deformed sphere for {0} found, creating one", body.theName);

                mesh = UnityEngine.GameObject.Instantiate(sphereMesh) as Mesh;   // Creates a duplicate of sphereMesh, a pristine

                Vector3[] vertices = mesh.vertices;
                float timer = Time.realtimeSinceStartup;

                if (body.pqsController != null)
                {
                    for (int i = 0; i < vertices.Length; ++i)
                    {
                        // let the user budget time.  This should be unnecessary using
                        // the default sphere settings
                        if (i % 250 == 0)
                            if (Time.realtimeSinceStartup - timer > Settings.MESH_DEFORM_MAX_DELTA)
                            {
                                yield return 0;
                                timer = Time.realtimeSinceStartup;
                            }

                        Vector3d vertex = vertices[i];

                        // this is pretty close but not quite good enough.  Unfortunately, due
                        // to the resolution of the sphere mesh, it's possible for anomalous areas
                        // (like lone mountains or large isolated features) to come "between" two low parts
                        // of the overlay sphere.  
                        //vertices[i] = vertex * body.pqsController.GetSurfaceHeight((Vector3d)vertices[i]) / body.pqsController.radius;


                        // We'll query an area around the axis of the radial vector
                        // and take the highest point out of that bunch instead.
                        vertices[i] = vertex * GetAreaHeight(vertex) / body.pqsController.radius;
                    }
                } // otherwise vertex locations are already normalized and we needn't do anything

                mesh.vertices = vertices;
                mesh.Optimize();
                mesh.RecalculateBounds();
                

                // store mesh in cache
                cache[body].mesh = mesh;
                mf.sharedMesh = mesh;


#if DISABLE_WIREFRAME
                Wireframe = false;
#endif
                
                Log.Verbose("Created deformed mesh for {0}; stored in cache", body.theName);
            }

#if DEBUG
#if !DISABLE_WIREFRAME
            // update wireframe lines
            sphere.GetComponent<WireFrameLineRenderer>().Refresh();
#endif
#endif
            Log.Debug("Set transform.");
            gameObject.transform.parent = ScaledSpace.Instance.scaledSpaceTransforms.Single(t => t.name == body.name);
            gameObject.transform.localScale = Vector3.one * 1020f * ((float)(body.Radius / 6000000d) / transform.parent.localScale.x);
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.transform.rotation = gameObject.transform.parent.rotation;

            Log.Debug("Sphere scale is {0},{1},{2}", gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
            Log.Debug("Parent scale is {0},{1},{2}", gameObject.transform.parent.localScale.x, gameObject.transform.parent.localScale.y, gameObject.transform.parent.localScale.z);

            Log.Verbose("Finished updating sphere mesh");

            _profile.End();
        }


        
        



        /// <summary>
        /// The custom overlay shader will apply a highlighting effect to
        /// areas on the biome map which match biomeColor
        /// </summary>
        /// <param name="biomeColor"></param>
        private void SetHighlightBiomeTarget(Color biomeColor)
        {
            materialOverlay.SetColor("_TargetColor", biomeColor);
            materialInspection.SetColor("_TargetColor", biomeColor);
            renderer.material.SetColor("_TargetColor", biomeColor);

        }



        /// <summary>
        /// Update highlighted biome based on user input
        /// </summary>
        public void Update()
        {
            if (IsBusy)
                return;

            //if (Input.GetKeyDown(KeyCode.V))
            //    Visible = true;


            if (referenceBody == null)
                return;

            if (!Visible)
            {
                currentBiome = "";
                SetHighlightBiomeTarget(Color.clear);
                Mode = OverlayMode.Normal;
                return;
            }


            // determine location of mouse on reference body
            var coords = CursorToCoordinates();  

            if (coords == null)
            {
                // don't highlight any biomes
                SetHighlightBiomeTarget(Color.clear);
                currentBiome = "";
            }
            else
            {
                var mapAttribute = Body.BiomeMap.GetAtt(coords.latitude * Mathf.Deg2Rad, coords.longitude * Mathf.Deg2Rad);
                SetHighlightBiomeTarget(mapAttribute.mapColor);

                currentBiome = mapAttribute.name;

                //if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))
                if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
                {
                    if (Mode != OverlayMode.Inspecting)
                    {
                        Mode = OverlayMode.Inspecting;
                        Log.Verbose("Overlay switched to inspection mode");
                    }
                }
                else if (Mode != OverlayMode.Normal)
                {
                    Mode = OverlayMode.Normal;
                    Log.Verbose("Overlay switched to normal mode");
                }
            }

            reticle.UpdateReticle(coords);
        }








        #region initialization

        /// <summary>
        /// Create a UV sphere which the player will see in MapView.
        /// http://wiki.unity3d.com/index.php/ProceduralPrimitives
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="nbLong"></param>
        /// <param name="nbLat"></param>
        /// <returns></returns>
        private Mesh GenerateSphere(float radius = 1f, int nbLong = 24, int nbLat = 16)
        {
            Mesh mesh = new Mesh();// filter.mesh;
            mesh.Clear();

            #region Vertices
            Vector3[] vertices = new Vector3[(nbLong + 1) * nbLat + 2];
            float _pi = Mathf.PI;
            float _2pi = _pi * 2f;

            vertices[0] = Vector3.up * radius;
            for (int lat = 0; lat < nbLat; lat++)
            {
                float a1 = _pi * (float)(lat + 1) / (nbLat + 1);
                float sin1 = Mathf.Sin(a1);
                float cos1 = Mathf.Cos(a1);

                for (int lon = 0; lon <= nbLong; lon++)
                {
                    float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
                    float sin2 = Mathf.Sin(a2);
                    float cos2 = Mathf.Cos(a2);

                    vertices[lon + lat * (nbLong + 1) + 1] = new Vector3(sin1 * cos2, cos1, sin1 * sin2) * radius;
                }
            }
            vertices[vertices.Length - 1] = Vector3.up * -radius;
            #endregion

            #region Normales
            Vector3[] normales = new Vector3[vertices.Length];
            for (int n = 0; n < vertices.Length; n++)
                normales[n] = vertices[n].normalized;
            #endregion

            #region UVs
            Vector2[] uvs = new Vector2[vertices.Length];
            uvs[0] = Vector2.up;
            uvs[uvs.Length - 1] = Vector2.zero;
            for (int lat = 0; lat < nbLat; lat++)
                for (int lon = 0; lon <= nbLong; lon++)
                    uvs[lon + lat * (nbLong + 1) + 1] = new Vector2((float)lon / nbLong, 1f - (float)(lat + 1) / (nbLat + 1));
            #endregion

            #region Triangles
            int nbFaces = vertices.Length;
            int nbTriangles = nbFaces * 2;
            int nbIndexes = nbTriangles * 3;
            int[] triangles = new int[nbIndexes];

            //Top Cap
            int i = 0;
            for (int lon = 0; lon < nbLong; lon++)
            {
                triangles[i++] = lon + 2;
                triangles[i++] = lon + 1;
                triangles[i++] = 0;
            }

            //Middle
            for (int lat = 0; lat < nbLat - 1; lat++)
            {
                for (int lon = 0; lon < nbLong; lon++)
                {
                    int current = lon + lat * (nbLong + 1) + 1;
                    int next = current + nbLong + 1;

                    triangles[i++] = current;
                    triangles[i++] = current + 1;
                    triangles[i++] = next + 1;

                    triangles[i++] = current;
                    triangles[i++] = next + 1;
                    triangles[i++] = next;
                }
            }

            //Bottom Cap
            for (int lon = 0; lon < nbLong; lon++)
            {
                triangles[i++] = vertices.Length - 1;
                triangles[i++] = vertices.Length - (lon + 2) - 1;
                triangles[i++] = vertices.Length - (lon + 1) - 1;
            }
            #endregion

            mesh.vertices = vertices;
            mesh.normals = normales;
            mesh.uv = uvs;
            mesh.triangles = triangles;

            mesh.RecalculateBounds();
            mesh.Optimize();

            return mesh;
        }



        #endregion

        #region Properties
        internal Vector3 Scale
        {
            get 
            {
                return transform.localScale;
            }
            set
            {
                transform.localScale = value;
                Log.Debug("Sphere scale is now set to {0},{1},{2}", value.x, value.y, value.z);
            }
        }

        public CelestialBody Body
        {
            get { return referenceBody; }
        }

        public string HighlightedBiome
        {
            get { return currentBiome; }
        }

        public List<CBAttributeMap.MapAttribute> Biomes
        {
            get
            {
                if (Body == null || Body.BiomeMap == null || Body.BiomeMap.Map == null)
                    return new List<CBAttributeMap.MapAttribute>();

                return Body.BiomeMap.Attributes.ToList();
            }
        }


        /// <summary>
        /// Assign will set this flag when it's working an an assignment
        /// </summary>
        private bool IsBusy
        {
            get
            {
                return state != OverlayBackgroundState.Idle;
            }
        }


        private bool Visible
        {
            get
            {
                //return gameObject.activeInHierarchy;
                return gameObject.renderer.enabled;
            }
            set
            {
                if (value && IsBusy)
                {
                    Log.Error("Overlay is currently in state {0}; canoot be made visible.");
                    return;
                }

                //gameObject.SetActive(value);
                gameObject.renderer.renderer.enabled = value;
                reticle.Visible = value;

                Log.Debug("Sphere is now {0}", (value ? "visible" : "invisible"));

#if DEBUG
#if !DISABLE_WIREFRAME
                //sphere.GetComponent<WireFrameLineRenderer>().Visible = value;
                Wireframe = value;
#endif
#endif
            }
        }

#if DEBUG
        internal bool Wireframe
        {
            set
            {
                GetComponent<WireFrameLineRenderer>().Visible = value;
            }

            get
            {
                return GetComponent<WireFrameLineRenderer>().Visible;
            }
        }
#endif

        #endregion

        #region Events


        /// <summary>
        /// Our owner (BiomeOverlay) will call us when the situation 
        /// changes.  Appropriate response goes here
        /// </summary>
        /// <param name="mapVisible"></param>
        /// <param name="guiVisible"></param>
        public void OnMapViewChange(bool mapVisible, bool guiVisible)
        {
            Log.Verbose("Sphere OnMapViewChange: map {0}, gui {1}", mapVisible, guiVisible);
            Visible = mapVisible && guiVisible;
        }



        /// <summary>
        /// When user changes the thing they're looking at in MapView,
        /// this event gets raised
        /// </summary>
        /// <param name="mo"></param>
        public void OnPlanetariumTargetChange(MapObject mo)
        {
            Log.Verbose("PlanetariumTargetChange: {0}", mo.name);

            Assign(getTargetBody(mo));

        }
        #endregion

        #region helpers
        public static bool InexactMatch(Color first, Color second, float threshold = DEFAULT_COLOR_DIFFERENCE_THRESHOLD, bool matchAlpha = false)
        {
            for (int i = 0; i < (matchAlpha ? 4 : 3); ++i)
                if (Mathf.Abs(first[i] - second[i]) > threshold)
                    return false;

            return true;
        }


        /// <summary>
        /// Queries current body for height at the given radial vector plus
        /// an area around that vector's axis and returns maximum height
        /// discovered.
        /// </summary>
        /// <param name="radial"></param>
        /// <returns></returns>
        private double GetAreaHeight(Vector3d radial)
        {
            if (!Body)
                return 0d;

            if (Body.pqsController == null)
                return Body.Radius;

            double maxHeight = Body.pqsController.GetSurfaceHeight(radial.normalized);
            Quaternion lookDirection = Quaternion.LookRotation(radial);

            //Log.Verbose("angle step = {0}", Math.Min(Settings.SPHERE_LAT_SLICES, Settings.SPHERE_LONG_SLICES));

            for (float angle = 0; angle < 360f; angle += Math.Min(Settings.SPHERE_LAT_SLICES, Settings.SPHERE_LONG_SLICES))
            {
                Vector3d newRadialPoint = radial + (Vector3d)(lookDirection * Quaternion.AngleAxis(angle, Vector3.forward) * Vector3.up) * (Settings.SPHERE_LAT_SLICES / 180d);
                //Log.Verbose("new radial: {0},{1},{2} (old was {3},{4},{5})", newRadialPoint.x, newRadialPoint.y, newRadialPoint.z, radial.x, radial.y, radial.z);

                maxHeight = Math.Max(maxHeight, Body.pqsController.GetSurfaceHeight(newRadialPoint/*.normalized*/));
            }

            return maxHeight;
        }



        private static CelestialBody getTargetBody(MapObject target)
        {
            if (!target) return null;

            switch (target.type)
            {
                case MapObject.MapObjectType.CELESTIALBODY:
                    return target.celestialBody;
                case MapObject.MapObjectType.MANEUVERNODE:
                    return target.maneuverNode.patch.referenceBody;
                case MapObject.MapObjectType.VESSEL:
                    return target.vessel.mainBody;
                case MapObject.MapObjectType.NULL:
                    return null;
                default:
                    Log.Warning("Unhandled MapObjectType in getTargetBody; investigate any version changes");
                    return null;
            }
        }



        //keeps angles in the range 0 to 360
        private static double ClampDegrees360(double angle)
        {
            angle = angle % 360.0;
            if (angle < 0) return angle + 360.0;
            else return angle;
        }



        //keeps angles in the range -180 to 180
        private static double ClampDegrees180(double angle)
        {
            angle = ClampDegrees360(angle);
            if (angle > 180) angle -= 360;
            return angle;
        }



        //private static Coordinates CursorToCoordinates(CelestialBody body)
        //{
        //    Ray mouseRay = PlanetariumCamera.Camera.ScreenPointToRay(Input.mousePosition);
        //    mouseRay.origin = ScaledSpace.ScaledToLocalSpace(mouseRay.origin);
        //    Vector3d relOrigin = mouseRay.origin - body.position;
        //    Vector3d relSurfacePosition = Vector3d.zero;
        //    double curRadius = (body.pqsController != null ? body.pqsController.radiusMax : body.Radius * 1.15);

        //    if (PQS.LineSphereIntersection(relOrigin, mouseRay.direction, curRadius, out relSurfacePosition))
        //    {
        //        Vector3d surfacePoint = body.position + relSurfacePosition;

        //        return new Coordinates(body.GetLatitude(surfacePoint), ClampDegrees180(body.GetLongitude(surfacePoint)));
        //    }

        //    return null;
        //}


        /// <summary>
        /// Determine coordinates of the overlay body the user is mousing over
        /// (if any).
        /// 
        /// Note: this function works in local coordinates because some precision point
        /// errors begin appearing the farther away from the vessel you get.  Specifically,
        /// inaccuracies in lat/lon from the CelestialBody's GetLat/Lon functions cause
        /// jittering at distant locations such as Eeloo if the player isn't nearby
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        private Coordinates CursorToCoordinates()
        {
            
            Ray mouseRay = PlanetariumCamera.Camera.ScreenPointToRay(Input.mousePosition);
            Vector3d relOrigin = mouseRay.origin - referenceScaledTransform.position;
            Vector3d relSurfacePosition = Vector3d.zero;
            double curRadius = ScaledSpace.InverseScaleFactor * (referenceBody.pqsController != null ? referenceBody.pqsController.radiusMax : referenceBody.Radius);

            if (PQS.LineSphereIntersection(relOrigin, mouseRay.direction, curRadius, out relSurfacePosition))
            {
                Vector3d surfacePoint = referenceScaledTransform.position + relSurfacePosition;
                Vector3d radial = (surfacePoint - referenceScaledTransform.position).normalized;
                
                double lat = Math.Asin(radial.y) * (double)Mathf.Rad2Deg;
                if (double.IsNaN(lat)) lat = 0.0;

                double lon = Math.Atan2(radial.z, radial.x) * (double)Mathf.Rad2Deg - referenceBody.directRotAngle;
                if (double.IsNaN(lon)) lon = 0.0;

                return new Coordinates(lat, lon);
            }

            //Log.Error("CursorToCoordinates: no body found");
            return null;
        }
        
        

        #endregion



        /// <summary>
        /// Prevent the specified biomes from being visible on the mapview
        /// overlay
        /// </summary>
        /// <param name="biomes"></param>
        public void MaskBiomes(List<CBAttributeMap.MapAttribute> biomes /* if empty, mask none */)
        {
            if (IsBusy)
            {
                Log.Verbose("MaskBiomes: Overlay is busy.  Wait till it's finished.");
                return;
            }

            var _profile = new ProfileTimer("MaskBiomes");

            if (biomes.Count == 0)
            {
                Log.Verbose("MaskBiomes: show all");
            }
            else foreach (var biome in biomes) Log.Verbose("MaskBiomes: mask {0} - {1}", biome.name, biome.mapColor);

            // work on a fresh copy of the biome map
            var texture = (Texture2D)UnityEngine.Texture2D.Instantiate(cache[Body].map);

            // note: there is apparently some issues on certain hardware and
            // platforms where blitting from render target to render target
            // results in artifacts or corruption, so we'll give the users
            // a choice of which method they want to use (render targets being
            // the default)
            if (Settings.USE_RENDERTARGET_METHOD)
            {
                // PROFILER: RenderTargetMasking took 0.01039505 seconds
                var _rtProfile = new ProfileTimer("RenderTargetMasking");
                    RenderTargetMasking(texture, biomes);
                _rtProfile.End();
            }
            else // use the basic pixel-by-pixel access method
            {
                var _pixelProfile = new ProfileTimer("SetPixelMasking");
                    PixelMasking(texture, biomes);
                _pixelProfile.End();
            }

            // destroy the old texture and assign the fresh one
            UnityEngine.Texture.Destroy(renderer.material.mainTexture);
            renderer.material.mainTexture = texture;

            _profile.End();
        }



        public void MaskBiomes()
        {
            MaskBiomes(new List<CBAttributeMap.MapAttribute>());
        }



        /// <summary>
        /// Perform masking on cpu.  In my benchmark case with 2 experiments and 2
        /// biomes, this test performed approximately 90% slower than the render
        /// target masking.  However, it's more straightforward and less prone
        /// to issues that apparently appear when blitting from one
        /// RenderTexture to another on some hardware
        /// 
        /// note: benchmark average was 0.09208679 seconds
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="biomes"></param>
        private void PixelMasking(Texture2D texture, List<CBAttributeMap.MapAttribute> biomes)
        {
            var pixels = texture.GetPixels32(0);
            Color32[] targetColors = new Color32[biomes.Count];

            for (int i = 0; i < biomes.Count; ++i)
                targetColors[i] = (Color32)biomes[i].mapColor;

            for (int i = 0; i < texture.width * texture.height; ++i)
                for (int colorIndex = 0; colorIndex < targetColors.Length; ++colorIndex)
                    if (InexactMatch(pixels[i], targetColors[colorIndex]))
                    {
                        pixels[i] = Color.clear;
                        break;
                    }

            texture.SetPixels32(pixels, 0);
            texture.Apply(false);

            if (Settings.OUTPUT_DEBUG_IMAGES)
                texture.SaveToDisk("PixelMaskingResult");
        }



        /// <summary>
        /// In this masking method, we utilize the power of the GPU to apply
        /// a shader to the biome map which performs masking for us.  Doing
        /// this on the GPU and then copying results back to the texture is
        /// considerably faster than direct pixel access, especially in more
        /// complex cases.
        /// 
        /// 2 experiment, 2 biome benchmark took an average of 0.01039505 seconds
        /// </summary>
        /// <param name="target"></param>
        /// <param name="biomes"></param>
        private void RenderTargetMasking(Texture2D target, List<CBAttributeMap.MapAttribute> biomes)
        {

            // all right, here's the plan.  We have a shader that will set
            // the alpha channel of colors that match a target value to transparent.
            //
            // We're going to ping pong between two render targets until we've done
            // a pass for every target color.  Whichever RT ends up with the final
            // result, we'll copy those pixels into the target texture


            RenderTexture from = RenderTexture.GetTemporary(target.width, target.height, 0);
            RenderTexture to = null;
            int counter = 0;

            // fill "from" rt with initial data
            Graphics.Blit(target, from);

            foreach (var biome in biomes)
            {
                biomeMaskMaterial.SetColor("_TargetColor", biome.mapColor);
                to = RenderTexture.GetTemporary(from.width, from.height, 0);

                Graphics.Blit(from, to, biomeMaskMaterial);

                if (Settings.OUTPUT_DEBUG_IMAGES)
                {
                    target.ReadPixels(/* pixel space */ new Rect(0f, 0f, target.width, target.height), 0, 0);
                    target.SaveToDisk(string.Format("rt_pass_{0}.png", counter));
                }

                RenderTexture.ReleaseTemporary(from);
                from = to;
                to = null;

                ++counter;
            }

            RenderTexture.active = from;

            target.ReadPixels(/* pixel space */ new Rect(0f, 0f, target.width, target.height), 0, 0);
            target.Apply();

            
            if (Settings.OUTPUT_DEBUG_IMAGES)
                target.SaveToDisk("RenderTextureResult");

            // finished with the RTs
                RenderTexture.ReleaseTemporary(from);
        }



        #region Properties

        private OverlayMode currentMode = OverlayMode.Normal;
        private OverlayMode Mode
        {
            get
            {
                return currentMode;
            }

            set
            {
                if (value != currentMode)
                {
                    var currentTex = renderer.material.mainTexture;

                    switch (value)
                    {
                        case OverlayMode.Normal:
                            renderer.material = materialOverlay;
                            renderer.material.mainTexture = currentTex;
                            renderer.material.mainTexture.filterMode = FilterMode.Point;
                            break;

                        case OverlayMode.Inspecting:
                            renderer.material = materialInspection;
                            renderer.material.mainTexture = currentTex;
                            renderer.material.mainTexture.filterMode = FilterMode.Bilinear;
                            break;

                        default:
                            Log.Error("Unknown overlay mode");
                            break;
                    }

                    
                    currentMode = value;
                }
            }
        }

        #endregion




    }
}
