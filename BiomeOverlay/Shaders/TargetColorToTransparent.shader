﻿// Given a TargetColor, set pixels of that color
// to transparent.  Otherwise don't change color

Shader "Custom/TargetColorToTransparent" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TargetColor ("Target Color", Color) = (0.0, 0.0, 0.0, 0.0)
		_Tolerance ("Color Tolerance", Color) = (0.05, 0.05, 0.05, 0.05)
	}
	SubShader {
		//Tags { "Queue" = "Opaque" }
        LOD 100
    
        ZTest always
        //Blend SrcAlpha OneMinusSrcAlpha 
		Blend Off
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float4 _TargetColor;			// this color to be replaced with HighlightColor		
			float4 _Tolerance;				// maximum diff between color float values for them to be considered the same



			struct appdata 
			{
				float4 vertex 	: POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos 		: SV_POSITION;
				float2 uv 		: TEXCOORD0;
			};

			// ------------------------------------------
			//	Vertex Shader
			// ------------------------------------------
			v2f vert(appdata v)	
			{
                v2f o;

                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord.xy;

                return o;
			} // End vertex shader
			


			// ------------------------------------------
			//   Fragment Shader
			// ------------------------------------------
			fixed4 frag(v2f i) : COLOR
			{
				float4 textureColor = tex2D(_MainTex, i.uv);

				// compare textureColor to targetColor
				float4 result = abs(textureColor - _TargetColor);
				
				if (result.r < _Tolerance.r)
				if (result.g < _Tolerance.g)
				if (result.b < _Tolerance.b)
				{
					// got a match, hide this color
					textureColor.arb = 0;
					textureColor.g = 1;
				}
			
				
				return textureColor;

			} // End fragment shader
			
			ENDCG
		}
	} 
	
	FallBack "KSP/Unlit"
	Fallback "Diffuse"
}

