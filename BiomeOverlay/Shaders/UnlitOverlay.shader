﻿Shader "Custom/UnlitOverlay" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TargetColor ("Target Color", Color) = (1.0, 0.0, 1.0, 1.0)
		_HighlightColor ("Main Highlight Color", Color) = (1.0, 1.0, 1.0, 0.0)
		_AlternateHighlightColor ("Lerp Highlight Color", Color) = (1.0, 0.0, 0.0, 1.0)
		_Tolerance ("Color Tolerance", Color) = (0.05, 0.05, 0.05, 0.05)
		_Alpha ("Translucent Alpha", Float) = 1.0
		_Frequency ("Blink Frequency", Float) = 1.0
	}
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 100
    
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha 

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _Frequency;
			float4 _TargetColor;			// this color to be replaced with HighlightColor
			float4 _HighlightColor;			// this color replaces _TargetColor
			float4 _AlternateHighlightColor;// to better identify the selected biome, the shader will interpolate between
											// this color and _HighlightColor over time
											
			float4 _Tolerance;				// maximum diff between color float values for them to be considered the same
			float _Alpha;					// only for non-matching colors; alpha of highlight is specified in the highlight color itself
											// note: is combined with sampler alpha channel value, if it has one
			
			// note: if _HighlightColor alpha = 0, DO NOT HIGHLIGHT ANYTHING



			struct appdata 
			{
				float4 vertex 	: POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos 		: SV_POSITION;
				float2 uv 		: TEXCOORD0;
			};

			// ------------------------------------------
			//	Vertex Shader
			// ------------------------------------------
			v2f vert(appdata v)	
			{
                v2f o;

                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord.xy;

                return o;
			} // End vertex shader
			


			// ------------------------------------------
			//   Fragment Shader
			// ------------------------------------------
			fixed4 frag(v2f i) : COLOR
			{
				float4 textureColor = tex2D(_MainTex, i.uv);
				textureColor.a *= _Alpha;
				
				// highlighting enabled?
				if (textureColor.a > _Tolerance.a)
				if (_HighlightColor.a > _Tolerance.a) 
				{
					// highlighting is enabled, compare textureColor to targetColor
					float4 result = abs(textureColor - _TargetColor);
					
					if (result.r < _Tolerance.r)
					if (result.g < _Tolerance.g)
					if (result.b < _Tolerance.b)
					{
						float weight = (cos(_Time * _Frequency).a + 1) * 0.5;
						
						textureColor = lerp(_HighlightColor, _AlternateHighlightColor, weight);
					}
				} 
				
				return textureColor;

			} // End fragment shader
			
			ENDCG
		}
	} 
	
	FallBack "Diffuse"
}
