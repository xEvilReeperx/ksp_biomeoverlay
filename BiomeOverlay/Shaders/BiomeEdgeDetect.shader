﻿Shader "Custom/BiomeEdgeDetect" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TargetColor ("Target Color", Color) = (1.0, 0.0, 1.0, 1.0)
		_HighlightColor ("Main Highlight Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_AlternateHighlightColor ("Lerp Highlight Color", Color) = (1.0, 0.0, 0.0, 1.0)
		_Tolerance ("Color Tolerance", Color) = (0.05, 0.05, 0.05, 0.05)
		_Frequency ("Blink Frequency", Float) = 1.0
		_Width ("Texture Width", Float) = 32.0
		_Height ("Texture Height", Float) = 32.0
	}
	SubShader {
		Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 100
    
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha 

		Pass
		{
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float _Frequency;
			float _Width;
			float _Height;
			
			float4 _TargetColor;			// this color to be replaced with HighlightColor
			
			float4 _HighlightColor;			// this color replaces _TargetColor
			float4 _AlternateHighlightColor;// to better identify the selected biome, the shader will interpolate between
											// this color and _HighlightColor over time
											
			float4 _Tolerance;				// maximum diff between color float values for them to be considered the same

			
			// note: if _HighlightColor alpha = 0, DO NOT HIGHLIGHT ANYTHING



			struct appdata 
			{
				float4 vertex 	: POSITION;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f 
			{
				float4 pos 		: SV_POSITION;
				float2 uv 		: TEXCOORD0;
			};

			// ------------------------------------------
			//	Vertex Shader
			// ------------------------------------------
			v2f vert(appdata v)	
			{
                v2f o;

                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord.xy;

                return o;
			} // End vertex shader
			


			// ------------------------------------------
			//   Fragment Shader
			// ------------------------------------------
			fixed4 frag(v2f i) : COLOR
			{
				float4 textureColor = tex2D(_MainTex, i.uv);
				
				
				float4 adjacents[4];
			
				adjacents[0] = tex2D(_MainTex, i.uv - fixed2(0, 1 / _Height));	// up
				adjacents[1] = tex2D(_MainTex, i.uv + fixed2(1 / _Width, 0)); 	// right
				adjacents[2] = tex2D(_MainTex, i.uv + fixed2(0, 1 / _Height));	// down
				adjacents[3] = tex2D(_MainTex, i.uv - fixed2(1 / _Width, 0));	// left
						
				// highlighting enabled?
				if (_HighlightColor.a > _Tolerance.a)
				{				
					float4 result = abs(textureColor - _TargetColor);
					
					// if this pixel is the target color,,,
					if (result.r < _Tolerance.r)
					if (result.g < _Tolerance.g)
					if (result.b < _Tolerance.b)
					{
						// we've got a target!
						// logic: if any adjacent pixel ISNT the target color,
						// this pixel should be highlight color
					

						
						bool highlight = false;
						
						for (int i = 0; i < 4; ++i)
						{
							result = abs(adjacents[i] - _TargetColor);
							
							if (result.r > _Tolerance.r || result.g > _Tolerance.g || result.b > _Tolerance.b)
							{
								highlight = true;
								break;
							}
						}
						
						if (highlight == true)
						{
							float weight = (cos(_Time * _Frequency).a + 1) * 0.5;
							textureColor = lerp(_HighlightColor, _AlternateHighlightColor, weight);
						
							return textureColor;
							
							//return fixed4(1, 1, 1, 1);
						} else { // don't highlight.  This pixel will be blank
							return fixed4(0, 0, 1, 0);
						}
					} else { // not the target color.  this pixel is blank
						return fixed4(1, 0, 0, 0); // correct
					}
				} else {
					// blank everything
					return fixed4(1, 0, 1, 0);
				}
				
				
				return fixed4(0, 1, 0, 0);
				
			} // End fragment shader
			
			ENDCG
		}
	} 
	
	//FallBack "Diffuse"
}
