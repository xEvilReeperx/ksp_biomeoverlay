﻿Shader "Custom/MinAmbient" {
    Properties {
      _MainTex ("Main Color", Color) = (1, 0, 0, 1)
      _MinLuminance ("Minimum Luminance", Float) = 0.5
    }
    
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf SimpleLambert

	  float4 _SunPosition;
	  float _MinLuminance;
	  
	  
      half4 LightingSimpleLambert (SurfaceOutput s, half3 lightDir, half atten) {
          half NdotL = dot (s.Normal, lightDir);
          half4 c; 
          c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
          c.a = s.Alpha;
          
          // Luminance  (0.2126*R) + (0.7152*G) + (0.0722*B)
          half L = 0.2126 * c.r + 0.7152 * c.g + 0.0722 * c.b;
          
          // come up with a value N such that (r+n, g+n, b+n)'s L is exactly _MinLuminance
          
          float n = (2500 * _MinLuminance - 1060 * c.r - 3586 * c.g - 361 * c.b) / 5007;
          //half3 minColor = (c.r + n, c.g + n, c.b + n);
          
          //if (L < _MinLuminance)
          	//c.rgb = c.rgb + (n, n, n);
          	
          c.rgb = lerp(c.rgb, half3(c.rgb + n), clamp(_MinLuminance - L, 0, 1));
          
          	//c.rgb = (s.Albedo.r + n, s.Albedo.g + n, s.Albedo.b + n) * _LightColor0.rgb * (NdotL * atten * 2); //mix(c.rgb, minColor, clamp(1 - (L - _MinLuminance), 0, 1));
          
          //c.r = max(c.r, _MinAmbient.r);
          //c.g = max(c.g, _MinAmbient.g);
          //c.b = max(c.b, _MinAmbient.b);
          
          return c;
      }

      struct Input {
          float2 uv_MainTex;
      };
      float4 _MainTex;
      void surf (Input IN, inout SurfaceOutput o) {
          o.Albedo = _MainTex.rgb;
      }
      ENDCG
    }
    //Fallback "Diffuse"
  }