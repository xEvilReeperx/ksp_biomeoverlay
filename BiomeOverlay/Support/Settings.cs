﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace BiomeOverlay
{

    class Settings
    {
        //---------------------------------------------------------------------
        // DebugOptions
        //---------------------------------------------------------------------
        public static bool VERBOSE_LOGGING = true;
        public static bool OUTPUT_DEBUG_IMAGES = false;
        public static bool PROFILE_LOGGING = true;


        //---------------------------------------------------------------------
        // OverlaySphere
        //---------------------------------------------------------------------
        public static int SPHERE_LONG_SLICES = 36;
        public static int SPHERE_LAT_SLICES = 24;

            // Normal overlay mode
            public static Color32 OVERLAY_HIGHLIGHT_COLOR = new Color(1f, 1f, 1f, 0.33f);
            public static Color32 OVERLAY_ALTERNATE_HIGHLIGHT_COLOR = new Color(0.35f, 0.58f, 0.27f, 0.5f);
            public static float OVERLAY_HIGHLIGHT_ALPHA = 0.25f;
            public static float OVERLAY_FREQUENCY = 1.7f;
            public static bool USE_RENDERTARGET_METHOD = true;

            // Inspection overlay mode
            public static Color32 OVERLAY_INSP_HIGHLIGHT_COLOR = new Color(1f, 1f, 1f, 1f);
            public static Color32 OVERLAY_INSP_ALT_HIGHLIGHT_COLOR = new Color(1f, 0f, 0f, 0.66f);
            public static float OVERLAY_INSP_FREQUENCY = 1f;


        //---------------------------------------------------------------------
        // MapViewButton
        //---------------------------------------------------------------------
        public static float MAPBUTTON_FRAME_RATE = 25f; // currently unused


        //---------------------------------------------------------------------
        // Time budget options
        //---------------------------------------------------------------------
        public static float BIOME_MAP_MAX_DELTA = 0.05f;
        public static float MESH_DEFORM_MAX_DELTA = 0.05f;


        //---------------------------------------------------------------------
        // Customization
        //---------------------------------------------------------------------
        public static bool DISPLAY_BIOME_NAME = true;
        public static bool DISPLAY_SPACE_ALTITUDES = true; // should gui let user know exact atmosphere and space altitudes?
        public static float RETICLE_SCALE = 1f;
        public static float RETICLE_ANIMATION_SPEED = 1f; // how fast the reticle spins


        //---------------------------------------------------------------------
        // Other
        //---------------------------------------------------------------------
        private static string CONFIG_PATH = "GameData/BiomeOverlay/settings.cfg";



        /// <summary>
        /// Initialize settings with values from disk
        /// </summary>
        internal static void Init()
        {
            //string configFile = IOUtils.GetFilePathFor(typeof(ScienceManager), "ScienceManager.cfg");
            var cfgPath = KSPUtil.ApplicationRootPath + CONFIG_PATH;
            Log.Verbose("cfgPath = {0}", cfgPath);

            try {
                ConfigNode settings = ConfigNode.Load(cfgPath);

                Log.Verbose("Loaded settings: {0}", settings.ToString());

                Load_DebugOptions(settings.GetNode("DebugOptions"));
                Load_MapViewButton(settings.GetNode("MapViewButton"));
                Load_OverlaySphere(settings.GetNode("OverlaySphere"));
                Load_CustomizationOptions(settings.GetNode("CustomizationOptions"));
                Load_TimeBudgets(settings.GetNode("TimeBudgets"));

                Log.Verbose("Loaded settings.");
            } catch (Exception e)
            {
                Log.Error("There was an error trying to load {0}. Missing entries will use default settings. Error was {1}", cfgPath, e);
            }
        }



        private static void Load_DebugOptions(ConfigNode node)
        {
            // bool Verbose 
            // bool DebugBiomes 
            // bool PerformanceProfile

            Log.Debug("Loading debug options");


            VERBOSE_LOGGING = bool.Parse(node.GetValue("Verbose"));
            OUTPUT_DEBUG_IMAGES = bool.Parse(node.GetValue("DebugBiomes"));
            PROFILE_LOGGING = bool.Parse(node.GetValue("PerformanceProfile"));
        }



        private static void Load_MapViewButton(ConfigNode node)
        {
            // float frameRate 

            Log.Verbose("Loading MapView options");


            MAPBUTTON_FRAME_RATE = float.Parse(node.GetValue("frameRate"));
        }



        private static void Load_OverlaySphere(ConfigNode node)
        {
            // int LongitudeSlices = 36
            // int LatitudeSlices = 24
                        
            //NormalOverlay
            //{
            //    HighlightColor = 1, 1, 1, 0.33
            //    AltHighlight = 0.35, .58, .27, 0.5
            //    OverallAlpha = 0.25
            //    PulseFrequency = 1.7
            //    UseRenderTargetMasking = True
            //}
            //InspectionOverlay
            //{
            //    HighlightColor = 1, 1, 1, 0.66
            //    AltHighlight = 1, 0, 0, 1
            //    PulseFrequency = 2
            //}
            //}

            Log.Verbose("Loading OverlaySphere options");


            SPHERE_LONG_SLICES = int.Parse(node.GetValue("LongitudeSlices"));
            SPHERE_LAT_SLICES = int.Parse(node.GetValue("LatitudeSlices"));

            #region NormalOverlay

            Log.Verbose("  - normal overlay settings");
            ConfigNode norm = node.GetNode("NormalOverlay");
          
            OVERLAY_HIGHLIGHT_COLOR = new Color(OVERLAY_HIGHLIGHT_COLOR.r, OVERLAY_HIGHLIGHT_COLOR.g, OVERLAY_HIGHLIGHT_COLOR.b, OVERLAY_HIGHLIGHT_COLOR.a).ParseColor(norm.GetValue("HighlightColor"));
            OVERLAY_ALTERNATE_HIGHLIGHT_COLOR = new Color(OVERLAY_ALTERNATE_HIGHLIGHT_COLOR.r, OVERLAY_ALTERNATE_HIGHLIGHT_COLOR.g, OVERLAY_ALTERNATE_HIGHLIGHT_COLOR.b, OVERLAY_ALTERNATE_HIGHLIGHT_COLOR.a).ParseColor(norm.GetValue("AltHighlight"));
            OVERLAY_HIGHLIGHT_ALPHA = float.Parse(norm.GetValue("OverallAlpha"));
            OVERLAY_FREQUENCY = float.Parse(norm.GetValue("PulseFrequency"));
            USE_RENDERTARGET_METHOD = bool.Parse(norm.GetValue("UseRenderTargetMasking"));
            #endregion

            #region InspectionOverlay

            Log.Verbose("  - inspection overlay settings");
            ConfigNode insp = node.GetNode("InspectionOverlay");

            OVERLAY_INSP_HIGHLIGHT_COLOR = ((Color)OVERLAY_INSP_HIGHLIGHT_COLOR).ParseColor(insp.GetValue("HighlightColor"));
            OVERLAY_INSP_ALT_HIGHLIGHT_COLOR = ((Color)OVERLAY_INSP_ALT_HIGHLIGHT_COLOR).ParseColor(insp.GetValue("AltHighlight"));
            OVERLAY_INSP_FREQUENCY = float.Parse(insp.GetValue("PulseFrequency"));

            #endregion
        }

        private static void Load_CustomizationOptions(ConfigNode node)
        {
            // bool DisplayBiomeNames
            // bool DisplayAltitudes
            // float ReticleScale
            // float ReticleAnimationSpeed

            Log.Verbose("Loading customization options");

            DISPLAY_BIOME_NAME = bool.Parse(node.GetValue("DisplayBiomeNames"));
            DISPLAY_SPACE_ALTITUDES = bool.Parse(node.GetValue("DisplayAltitudes"));
            RETICLE_SCALE = float.Parse(node.GetValue("ReticleScale"));
            RETICLE_ANIMATION_SPEED = float.Parse(node.GetValue("ReticleAnimationSpeed"));
        }


        private static void Load_TimeBudgets(ConfigNode node)
        {
            // float BiomeMapMaxDelta 
            // float MeshDeformMaxDelta

            Log.Verbose("Loading time budgets");

            BIOME_MAP_MAX_DELTA = float.Parse(node.GetValue("BiomeMaxMapDelta"));
            MESH_DEFORM_MAX_DELTA = float.Parse(node.GetValue("MeshDeformMaxDelta"));
        }


        public static Material LocateMaterial(string resourceName, params string[] backups)
        {
            var possibilities = new Dictionary<string /* resource name */, string /* what to display in log */>();
            possibilities.Add(resourceName, string.Format("LocateMaterial: Creating shader material '{0}'", resourceName));

            foreach (var backup in backups)
                possibilities.Add(backup, string.Format("LocateMaterial: Attempting backup shader '{0}'", backup));

            foreach (var resource in possibilities)
            {
                Log.Verbose(resource.Value);

                // check embedded resources
                var stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(resource.Key);

                if (stream != null)
                {
                    // found in our resources.  Extract and create
                    try
                    {
                        return new Material(new System.IO.StreamReader(stream).ReadToEnd());
                    }
                    catch (Exception e)
                    {
                        Log.Error("LocateMaterial: Failed to generate '{0}', although it was found!  Exception: {1}", resource.Key, e);
                        // bad news!  continue looking
                    }
                }
                else
                {
                    // not in our resources.  Check Shader lib
                    var shader = Shader.Find(resource.Key);

                    if (shader != null)
                    {
                        try
                        {
                            return new Material(shader);
                        }
                        catch (Exception e)
                        {
                            Log.Error("Failed to create material with non-embedded shader '{0}'!  Exception: {1}", resource.Key, e);
                        }
                    }
                    else
                    {
                        Log.Error("LocateMaterial: Failed to find '{0}'.  Proceeding to next fallback.", resource.Key);
                    }
                }
            }

            // if we manage to get to this point, no appropriate material was found at all!
            Log.Error("LocateMaterial: Failed to find any appropriate shader!");
            return null;
        }
    }


    

    public static class MyExtensions
    {
        public static UnityEngine.Color ParseColor(this UnityEngine.Color color, string str)
        {
            //Log.Debug("Parsing {0} into Vector4", str);

            Vector4 v4 = KSPUtil.ParseVector4(str);

            //Log.Debug("Got {0}, {1}, {2}, {3}", v4.x, v4.y, v4.z, v4.w);

            return new Color(v4.x, v4.y, v4.z, v4.w);
        }



        /// <summary>
        /// Saves texture into plugin dir with supplied name.
        /// Precondition: texture must be readable
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="name"></param>
        public static bool SaveToDisk(this UnityEngine.Texture2D texture, string name)
        {
            if (!name.EndsWith(".png"))
                name += ".png";

            try
            {
                Log.Verbose("Saving a {0}x{1} texture as '{2}'", texture.width, texture.height, name);

                System.IO.FileStream file = new System.IO.FileStream(KSPUtil.ApplicationRootPath + "/GameData/BiomeOverlay/textures/" + name, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
                System.IO.BinaryWriter writer = new System.IO.BinaryWriter(file);
                writer.Write(texture.EncodeToPNG());
                writer.Close();
                file.Close();

                Log.Verbose("Texture saved as {0} successfully.", name);
                return true;
            } catch (Exception e)
            {
                Log.Error("Failed to save texture '{0}' due to {1}", name, e);
                return false;
            }
        }
    }   
}
