﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BiomeOverlay.Support
{
    class BiomeMapGenerator
    {
        // private variables
        private Texture2D map;              // finished result


    //#####################################################################
    //  -- Begin implementation
    //#####################################################################
        

        /// <summary>
        /// A straightforward way to enable the Overlay to generate
        /// unaliased biome maps over a series of frames, distributing
        /// the work (and reducing the noticeable lag).  This function
        /// took 0.5-1 second to complete on my computer pre-coroutine.
        /// 
        /// Why don't we just grab the BiomeMap directly from the body
        /// you say?  Sadly it appears that some aliasing or compression
        /// is being done which distorts the colors at the edges of 
        /// biomes.  With the highlight shader it becomes very apparent
        /// when those inexact matches are missed, so either the highlight
        /// shader has to be very tolerant of inexact colors -- bad for
        /// futureproofing against huge biome numbers -- or we need to
        /// clean it up somehow.  The technique I'm using for this is 
        /// really simple.
        /// 
        /// We'll go through each pixel on the biome map we're generating
        /// and project its xy into lat/long.  Then we can query the
        /// original biome map for which biome is there and get a nice 
        /// clean result.
        /// </summary>
        /// <param name="body"></param>
        /// <returns></returns>
        public System.Collections.IEnumerator Create(CelestialBody body)
        {
            map = null;

            if (body == null)
            {
                Log.Error("BiomeMapGenerator: invalid body");
                yield break;
            }

            if (body.BiomeMap.Map == null)
            {
                Log.Error("BiomeMapGenerator: no biome map on {0}", body.name);
                yield break;
            }

            var _profile = new ProfileTimer("BiomeMapProjection");


            // actual work starts here
            Texture2D cleanMap = new Texture2D(body.BiomeMap.Map.width, body.BiomeMap.Map.height, TextureFormat.ARGB32, false);
            cleanMap.filterMode = FilterMode.Point;
           

            float timer = Time.realtimeSinceStartup;
            Color32[] pixels = cleanMap.GetPixels32();

            for (int y = 0; y < cleanMap.height; ++y)
            {
                for (int x = 0; x < cleanMap.width; ++x)
                {
                    // convert x and y into uv coordinates
                    float u = (float)x / cleanMap.width;
                    float v = (float)y / cleanMap.height;

                    // convert uv coordinates into latitude and longitude
                    // (BiomeMaps are spherical coordinates in 
                    // equirectangular projection)
                    double lat = Math.PI * v - Math.PI * 0.5;
                    double lon = 2d * Math.PI * u + Math.PI * 0.5;

                    // set biome color in our clean texture
                    pixels[y * cleanMap.width + x] = (Color32)body.BiomeMap.GetAtt(lat, lon).mapColor;
                }

                // pause periodically
                if (Time.realtimeSinceStartup - timer > Settings.BIOME_MAP_MAX_DELTA)
                {
                    yield return null;
                    timer = Time.realtimeSinceStartup;
                }
            }

            cleanMap.SetPixels32(pixels);
            cleanMap.Apply();

            if (Settings.OUTPUT_DEBUG_IMAGES)
                cleanMap.SaveToDisk(body.name + "_cleanMap.png");

            map = cleanMap;
            // actual work ends

            _profile.End();
        }


        // properties
        public Texture2D Result
        {
            get
            {
                return map;
            }
        }
    }
}
