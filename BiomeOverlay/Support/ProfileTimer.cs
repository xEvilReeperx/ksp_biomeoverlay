﻿using System.Diagnostics;
using UnityEngine;

namespace BiomeOverlay
{
    public class ProfileTimer
    {
        private float startTime = 0f;
        private string timerName = string.Empty;
        private bool running = true;

        public ProfileTimer(string name)
        {
            startTime = Time.realtimeSinceStartup;
            timerName = name;
        }

        public void End()
        {
            if (running)
            {
                Log.Profile(string.Format("{0} took {1} seconds", timerName, Time.realtimeSinceStartup - startTime));
                running = false;
            }
        }

        public void Start()
        {
            startTime = Time.realtimeSinceStartup;
            running = true;
        }

        ~ProfileTimer()
        {
            if (running)
            {
                Log.Warning("Profile test {0} was not ended", timerName);
                End();
            }
        }
    }
}
