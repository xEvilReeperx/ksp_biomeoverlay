﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using KSP.IO;
using System.IO;
using Toolbar;

namespace BiomeOverlay
{

    

    [KSPAddon(KSPAddon.Startup.Flight, false)]
    public class BiomeOverlay : MonoBehaviour
    {
        //---------------------------------------------------------------------
        // Constants
        //---------------------------------------------------------------------
        const string TOOLBAR_ICON_NAME = "toolbar";
       

        //---------------------------------------------------------------------
        // GUI
        //---------------------------------------------------------------------
        private IButton toolbarButton;
        private UI.OptionsWindow optionsWindow;
        GUIStyle myStyle;

        bool uiVisible = true;
        
        Texture2D testTexture = null;

                

        //---------------------------------------------------------------------
        // Overlay sphere
        //---------------------------------------------------------------------
        Overlay overlay = null;


        //---------------------------------------------------------------------
        // Events and callbacks
        //---------------------------------------------------------------------
        System.Collections.IEnumerator startRoutine = null;



        // Why not have interested objects register for their
        // own MapView events directly?  They'd have to also
        // manage their own GUI state and register for On[Show/Hide]UI.
        // It's cleaner to have that functionality here at the
        // top level and just let those objects know what the state is
        // so they can respond to it as appropriate
        public delegate void MapCallbackHandler(bool isMapView, bool guiEnabled);
        public event MapCallbackHandler OnMapViewChange;







    //#####################################################################
    //  -- Begin implementation
    //#####################################################################


        /// <summary>
        /// Called just before Update loop begins
        /// </summary>
        public void Start()
        {
            startRoutine = DelayedStart();
        }



        /// <summary>
        /// For test purposes.  No functionality
        /// </summary>
        public void PersistenceLoad()
        {
            Log.Warning("PersistenceLoad called");
        }



        /// <summary>
        /// Prepare to run
        /// </summary>
        /// <returns></returns>
        private System.Collections.IEnumerator DelayedStart()
        {
            // R&D doesn't seem to be ready just as the script
            // is starting, so we'll wait for it
            while (ResearchAndDevelopment.Instance == null)
                yield return null;


            //Log.Verbose("Modifying tech tree");

            //foreach (var part in PartLoader.LoadedPartsList)
            //{
            //    var techNode = ResearchAndDevelopment.Instance.GetTechState(part.TechRequired);

            //    if (techNode != null)
            //        if (ResearchAndDevelopment.GetTechnologyState(techNode.techID) == RDTech.State.Available)
            //        {
            //            if (!ResearchAndDevelopment.PartModelPurchased(part) && !techNode.partsPurchased.Contains(part))
            //            {
            //                //Log.Debug("Part {0} wasn't purchased; purchasing it", part.title);
            //                techNode.partsPurchased.Add(part);
            //                ResearchAndDevelopment.Instance.SetTechState(techNode.techID, techNode);
            //            }
            //        }
            //        else
            //        {
            //            //Log.Verbose("part {0}'s tech {1} wasn't purchased but it's not unlocked; skipping", part.title, techNode.techID);
            //        }
            //}




            Log.Verbose("Starting");




            // ----- Basic initialization -------- //
            Settings.Init();
            //Audio.Instance.Init(); // unnecessary; first call to dereference instance will trigger load

            // Register for events
            Log.Verbose("Registering for events");
            GameEvents.onHideUI.Add(OnHideUI);
            GameEvents.onShowUI.Add(OnShowUI);

            MapView.OnEnterMapView += OnEnterMapView;
            MapView.OnExitMapView += OnExitMapView;
            

            // Set up GUI style
            myStyle = new GUIStyle();

            // set up textures
            testTexture = GameDatabase.Instance.GetTextureIn("BiomeOverlay/icons/", "test", false);


            // setup toolbar
            toolbarButton = ToolbarManager.Instance.add("BiomeOverlay", "BiomeOverlay");
            toolbarButton.TexturePath = "BiomeOverlay/icons/toolbar";
            toolbarButton.ToolTip = "Open Overlay";
            toolbarButton.Text = "Biome Overlay";
            toolbarButton.OnClick += OnToolbarClick;
            
             

            // create the overlay sphere and reticle
            //var overlay = Overlay.Create();
            var overlay = gameObject.AddComponent<Overlay>();

            // to prevent OverlaySphere from knowing about us,
            // we'll connect its events ourselves
            OnMapViewChange += overlay.OnMapViewChange;


            // Create GUI, set up controls and contents, etc
            optionsWindow = gameObject.AddComponent<UI.OptionsWindow>();
            optionsWindow.Initialize(overlay);
            OnMapViewChange += optionsWindow.OnMapViewChange;
          
        }



        /// <summary>
        /// No purpose
        /// </summary>
        public void Awake()
        {

        }



        /// <summary>
        /// Remove event handlers when destroyed
        /// </summary>
        public void OnDestroy()
        {
            Log.Verbose("BiomeOverlay OnDestroy");

            Audio.Instance.Unload();
            Log.Verbose("unloaded audio");

            Log.Verbose("removing events");
            if (optionsWindow != null) OnMapViewChange -= optionsWindow.OnMapViewChange;
            if (overlay != null) OnMapViewChange -= overlay.OnMapViewChange;
            MapView.OnExitMapView -= OnExitMapView;
            MapView.OnEnterMapView -= OnEnterMapView;
            
            GameEvents.onHideUI.Remove(OnHideUI);
            GameEvents.onShowUI.Remove(OnShowUI);
            Log.Verbose("Removed events");

            toolbarButton.Destroy();
            Log.Verbose("removed toolbar button");

            /*GameObject.DestroyImmediate(overlay);*/ overlay = null;
            Log.Verbose("BiomeOverlay finished destruction");
        }



        /// <summary>
        /// Called by Unity for GUI rendering and logic
        /// </summary>
        public void OnGUI()
        {
            if (startRoutine != null)
            {
                if (!startRoutine.MoveNext())
                    startRoutine = null;

                return;
            }

            if (!testTexture)
            {
                //Log.Error("No testTexture in OnGUI!");
                return;
            }

            // indicator button
            GUI.Button(new Rect(10, 400, 32, 32), testTexture);

            
        }



        /// <summary>
        /// Physics frame
        /// </summary>
        private void FixedUpdate()
        {

        }



        /// <summary>
        /// Called once per frame by Unity
        /// </summary>
        public void Update()
        {
            
        }


       

        #region Event handling

        /// <summary>
        /// Event handler
        /// </summary>
        public void OnHideUI()
        {
            uiVisible = false;
            OnMapViewChange(MapView.MapIsEnabled, uiVisible);
        }



        /// <summary>
        /// Event handler
        /// </summary>
        public void OnShowUI()
        {
            uiVisible = true;

            if (OnMapViewChange != null)
            {
                OnMapViewChange(MapView.MapIsEnabled, uiVisible);
            }
            else Log.Error("OnMapViewChange event is empty");
        }



        public void OnEnterMapView()
        {
            Log.Debug("Entered map view");
            OnMapViewChange(MapView.MapIsEnabled, uiVisible);
        }

        public void OnExitMapView()
        {
            Log.Debug("Left map view");
            OnMapViewChange(MapView.MapIsEnabled, uiVisible);
        }



        /// <summary>
        /// Event handler for blizzy toolbar button click
        /// </summary>
        /// <param name="e"></param>
        public void OnToolbarClick(ClickEvent e)
        {
            Log.Debug("Toolbar clicked!");
        }

#endregion
    }
}


#if DEBUG
    //This will kick us into the save called default and set the first vessel active
    [KSPAddon(KSPAddon.Startup.MainMenu, false)]
    public class Debug_AutoLoadQuicksaveOnStartup : UnityEngine.MonoBehaviour
    {
        public static bool first = true;
        public void Start()
        {
            if (first)
            {
                first = false;
                HighLogic.SaveFolder = "debug";
                var game = GamePersistence.LoadGame("quicksave", HighLogic.SaveFolder, true, false);
                if (game != null && game.flightState != null && game.compatible)
                {
                    FlightDriver.StartAndFocusVessel(game, game.flightState.activeVesselIdx);
                }
                CheatOptions.InfiniteFuel = true;
            }
        }
    }
#endif