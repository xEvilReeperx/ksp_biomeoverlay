﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BiomeOverlay
{
    /// <summary>
    /// A collection of various UI sounds.  Makes more sense to keep them
    /// separate from individual UI windows so that it's easier to reuse
    /// them and cuts down on code replication
    /// </summary>
    class Audio 
    {
        private static float TIMEOUT_DURATION = 2f;
        private static readonly string[] SOUND_PATHS = {
                                                           "BiomeOverlay/sounds/click1.wav",
                                                           "BiomeOverlay/sounds/click2.wav" 
                                                        };


        private UnityEngine.AudioClip[] clips;
        private GameObject gameObject;
        private static Audio instance;

        private Audio() { }

        public void Unload()
        {
            foreach (var clip in clips)
                UnityEngine.AudioClip.Destroy(clip);

            if (gameObject != null)
                UnityEngine.GameObject.Destroy(gameObject);

            instance = null;
        }

        public static Audio Instance
        {
            get 
            {
                if (instance == null)
                {
                    instance = new Audio();
                    instance.Init();
                }

                return instance;
            }
        }



        public void Init()
        {
            Log.Verbose("Initializing audio");

            LoadSounds();

            gameObject = new GameObject("BiomeOverlay.Audio.gameObject");
            gameObject.AddComponent<AudioSource>();

            Log.Verbose("Finished initialization audio");

        }



        /// <summary>
        /// Load sounds using WWW.  GameDatabase.GetAudioClip() seems to
        /// load all sounds as 3D.  That's normally okay except that the
        /// camera is almost always moving, so even if you use the main
        /// camera as an AudioSource you still get some artifacts where
        /// the sound itself seems to be on one side or the other, depending
        /// on how the camera view is positioned relative to the vessel's
        /// prograde direction
        /// </summary>
        private void LoadSounds()
        {
            clips = new AudioClip[SOUND_PATHS.Length];

            Log.Debug("OS: {0}, ", System.Environment.OSVersion.Platform);

            for (int index = 0; index < clips.Length; ++index)
            {
                string path = KSPUtil.ApplicationRootPath + "GameData/" + SOUND_PATHS[index];
                Log.Verbose("Loading sound {0}", path);

                // windows requires three slashes.  see:
                // http://docs.unity3d.com/Documentation/ScriptReference/WWW.html
                if (Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    path = "file:///" + path;
                }
                else path = "file://" + path;

                Log.Debug("sound path: {0}, escaped {1}", path, System.Uri.EscapeUriString(path));

                // WWW.EscapeURL doesn't seem to work all that great.  I couldn't get
                // AudioClips to come out of it correctly.  Non-escaped local urls
                // worked just fine but the docs say they should be escaped and this
                // works so I think it's the best solution currently
                //WWW clipData = new WWW(WWW.EscapeURL(path));
                WWW clipData = new WWW(System.Uri.EscapeUriString(path));

                float start = Time.realtimeSinceStartup;

                while (!clipData.isDone && Time.realtimeSinceStartup - start < TIMEOUT_DURATION)
                {  
                }

                if (!clipData.isDone)
                    Log.Error("Audio.LoadSounds() - timed out in {0} seconds", Time.realtimeSinceStartup - start);

                try
                {
                    clips[index] = clipData.GetAudioClip(false, false, AudioType.WAV);

                    if (clips[index] == null)
                        Log.Error("Error retrieving audio clip data: {0}", clipData.error);

                } catch (Exception)
                {
                    Log.Error("Error retrieving audio clip from clipData");
                }
                if (!clips[index])
                    Log.Error("Failed to load audio clip #{0}: {1}", index, SOUND_PATHS[index]);
            }

            Log.Verbose("Finishing loading audio clips.");
        }



        public void PlayClip(int index = 0, AudioSource src = null)
        {
            if (index < 0 || index >= clips.Length)
            {
                Log.Warning("PlayClip: index {0} is out of range", index);
                return;
            }
            else
            {
                try
                {
                    if (clips[index] != null)
                    {
                        if (src != null)
                        {
                            src.PlayOneShot(clips[index], GameSettings.UI_VOLUME);
                        }
                        else
                        {
                            gameObject.transform.position = Camera.main.transform.position;
                            gameObject.audio.PlayOneShot(clips[index], GameSettings.UI_VOLUME);
                        }
                    }
                    else Log.Warning("PlayClick failed: clip #{0} does not exist", index);
                } catch (Exception e)
                {
                    // note: there seems to be a bug that occurs when switching out of
                    // flight scene that I need to track down
                    Log.Error("An exception while trying to PlayClip {0}: {1}", index, e);
                }
            }
        }
    }
}
